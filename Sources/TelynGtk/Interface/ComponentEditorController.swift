import Foundation
import Gdk
import Gtk
import GtkKit
import OpenCombine
import TelynCore
import TelynViewModel

public class ComponentEditorController: WidgetController,  ComponentWidgetDelegate, SideDetailControllerDelegate {

	public override var widgetName: String? {
		return "component_editor"
	}

	public override var bundle: Bundle? {
		return Bundle.module
	}

	public lazy var componentWidget: ComponentWidget = child(named: "component_widget")

	public var spawnNodeRightClickGesture: GestureMultiPress!

	public lazy var linkOverlay: LinkOverlay = child(named: "link_overlay")

	public var linkGesture: LinkGesture!

	lazy var appMenuItem = BarButtonItem(iconName: "open-menu-symbolic")

	public var viewModel = ComponentEditorViewModel()

	var cancellables = Set<AnyCancellable>()

	public override func widgetDidLoad() {
		componentWidget.delegate = self
		viewModel.$primaryComponent.onMain { [unowned self] (component) in
			componentWidget.component = component
		}.store(in: &cancellables)
		viewModel.$selectedElement.onMain { [unowned self] (element) in
			componentWidget.selected = element
		}.store(in: &cancellables)

		viewModel.title.bindAsTitle(of: headerbarItem).store(in: &cancellables)
		viewModel.subtitle.bindAsSubtitle(of: headerbarItem).store(in: &cancellables)

		spawnNodeRightClickGesture = GestureMultiPress(widget: widget)
		spawnNodeRightClickGesture.button = 3
		spawnNodeRightClickGesture.onPressed(handler: onRightClick(_:_:x:y:))

		linkGesture = LinkGesture(widget: widget)
		linkGesture.onBegin(onLinkBegin(at:))
		linkGesture.onUpdate(onLinkUpdate(at:))
		linkGesture.onEnd(onLinkEnd(at:))
		linkGesture.onFailed(onLinkFailed)

		// Set the overlay to pass through events
		linkOverlay.onRealize { (linkOverlay) in
			linkOverlay.window.passThrough = true
		}
		headerbarItem.startItems = [
			BarButtonItem(iconName: "value-increase-symbolic", onClick: onCreateNode)
		]
		headerbarItem.endItems = [
			appMenuItem,
			BarButtonItem(iconName: "document-print-symbolic", onClick: onPrint),
			BarButtonItem(iconName: "ruler-symbolic", onClick: onSelectMeasurements)
		]
		viewModel.hasInstructions.onMain() { [unowned self] (hasInstructions) in
			generateAppMenu(hasInstructions: hasInstructions)
		}.store(in: &cancellables)

		keyActions = [
			KeyAction(input: "n", modifierFlags: .control, action: onCreateNode),
			KeyAction(input: "m", modifierFlags: .control, action: onSelectMeasurements),
			KeyAction(input: "m", modifierFlags: [.control, .shift], action: onOpenMeasurementManager),
			KeyAction(input: "b", modifierFlags: .control, action: onOpenBlockManager),
			KeyAction(input: "p", modifierFlags: .control, action: onPrint),
			KeyAction(input: "i", modifierFlags: .control, action: onViewInstructions),
			KeyAction(input: "i", modifierFlags: [.control, .shift], action: onAddInstructions),
			KeyAction(input: "h", modifierFlags: .control, action: onOpenManual)
		]
	}

	public func componentWidget(_ widget: ComponentWidget, didSelectElement element: AnyObject?, dueToEventAt: CGPoint) {
		print("Component widget selected element: \(element)")
		viewModel.selected(element: element)
	}

	public func onCreateNode() {
		viewModel.createNode()
	}

	public func onRightClick(_ gesture: GestureMultiPressRef,_ nClicks: Int, x: Double, y: Double) {
		guard nClicks == 1 else {
			return
		}
		if onHandleSpawnPopup(x: x, y: y) {
			spawnNodeRightClickGesture.set(state: .claimed)
		}
	}

	public func onHandleSpawnPopup(x: Double, y: Double) -> Bool {
		let point = CGPoint(x: x, y: y)
		let element = componentWidget.element(at: point)
		guard viewModel.shouldDisplayNodeSpawnPopup(forEventOver: element) else {
			return false
		}
		let menu = ActionMenu(children: [
			Action(title: "Add Node", handler: { [viewModel, componentWidget] () in
				let realPoint = componentWidget.renderContext.realPoint(for: point)
				viewModel.createNode(at: realPoint)
			})
		])
		menu.present(pointingTo: point, in: componentWidget)
		return true
	}

	public func onLinkBegin(at point: CGPoint) -> Bool {
		let element = componentWidget.element(at: point)
		let beginLink = viewModel.beginConnection(element: element, in: .primary)
		guard beginLink else {
			return false
		}
		linkOverlay.beginLink(at: point)
		return true
	}

	public func onLinkUpdate(at point: CGPoint) {
		let element = componentWidget.element(at: point)
		let linked = viewModel.updateConnection(to: element, in: .primary)
		linkOverlay.updateLink(at: point, canLink: linked)
	}

	public func onLinkEnd(at point: CGPoint)  {
		linkOverlay.endLink()
		let element = componentWidget.element(at: point)
		let actions = viewModel.finishConnection(to: element, in: .primary)
		guard !actions.isEmpty else {
			return
		}
		let menu = ActionMenu(children: actions.map { (action) -> GtkKit.Action in
			return GtkKit.Action(title: action.title, handler: action.action)
		})
		menu.present(pointingTo: point, in: componentWidget)
	}

	public func onLinkFailed() {
		componentWidget.dragRecogniser.set(state: .denied)
	}

	public func sideDetailController(_ sideDetailController: SideDetailController, displayed detailChild: WidgetController) {
		guard let selected = viewModel.selectedElement else {
			return
		}
		var bounds: RealRect? = nil
		if let node = selected as? Node {
			bounds = RealRect(origin: node.position, size: RealSize(width: 0, height: 0))
		} else if let line = selected as? Line {
			bounds = line.bounds
		} else if let constraint = selected as? Constraint {
			bounds = constraint.bounds
		}
		guard bounds != nil else {
			return
		}
		componentWidget.bring(rect: bounds!, into: componentWidget.centerFrame, animationLength: 0.2)
	}

	public func onSelectMeasurements() {
		viewModel.selectMeasurements()
	}

	public func onPrint() {
		viewModel.onPrint()
	}

	public func onExportAsBlocks() {
		viewModel.exportBlocks()
	}

	public func onAddInstructions() {
		viewModel.addInstructions()
	}

	public func onViewInstructions() {
		viewModel.viewInstructions()
	}

	public func onOpenMeasurementManager() {
		viewModel.openMeasurementEditor()
	}

	public func onOpenBlockManager() {
		viewModel.openBlockLibrary()
	}

	public func onOpenManual() {
		openManual(from: self)
	}

	public func generateAppMenu(hasInstructions: Bool) {
		var actions: [GtkKit.MenuElement] = []
		actions.append(contentsOf: [
			Action(title: "Export As Blocks", handler: onExportAsBlocks),
			MenuSeparator(),
			Action(title: "Attach Instructions", handler: onAddInstructions)
		] as [GtkKit.MenuElement])
		if hasInstructions {
			actions.append(Action(title: "View Instructions", handler: onViewInstructions))
		}
		actions.append(MenuSeparator())
		actions.append(contentsOf: [
			Action(title: "Manage Measurements", handler: onOpenMeasurementManager),
			Action(title: "Manage Blocks", handler: onOpenBlockManager),
			MenuSeparator(),
			Action(title: "Help", handler: onOpenManual)
		] as [GtkKit.MenuElement])
		appMenuItem.menu = ActionMenu(children: actions)
	}

}


