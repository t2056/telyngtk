import Foundation
import GtkKit
import TelynCore
import TelynViewModel

public class PrintScene: GtkNavigatorCustomScene, PresentationDelegate {

	var printViewModel = PrintViewModel()

	public init() {
		super.init(viewModel: printViewModel)
	}

	public override func present(from controller: WidgetController) {
		guard let pattern = printViewModel.pattern else {
			return
		}
		let printRenderer = PatternPrintRenderer(for: pattern, using: printViewModel.measurementSet)
		let printInteractionController = PrintInteractionController()
		printInteractionController.printPageRenderer = printRenderer
		controller.present(printInteractionController)
	}

	public func presentationDidEnd(_ presentationController: PresentationController) {
		printViewModel.didComplete()
	}

}
