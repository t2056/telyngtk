import Foundation
import Gtk
import GtkKit
import TelynCore
import TelynViewModel

public class InstructionsImportScene: GtkNavigatorCustomScene {

	var instructionsImportViewModel = InstructionsImportViewModel()

	public init() {
		super.init(viewModel: instructionsImportViewModel)
	}

	public override func present(from controller: WidgetController) {
		let documentPicker = DocumentPickerController.forOpeningFile(ofType: .pdf, title: "Add Instructions", onFileSelected: addInstructions(_:))
		documentPicker.confirmButtonTitle = "Attach"
		documentPicker.onCancel = { [instructionsImportViewModel] () in
			instructionsImportViewModel.didComplete()
		}
		controller.present(documentPicker)
	}

	public func addInstructions(_ url: URL) {
		instructionsImportViewModel.selectedInstructions(withData: try? Data(contentsOf: url))
	}

}
