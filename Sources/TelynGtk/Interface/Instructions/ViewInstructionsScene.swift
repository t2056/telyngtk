import Foundation
import Gdk
import Gtk
import GtkKit
import TelynViewModel

public class ViewInstructionsScene: GtkNavigatorCustomScene {

	var viewInstructionsViewModel = ViewInstructionsViewModel()

	public init() {
		super.init(viewModel: viewInstructionsViewModel)
	}

	public override func present(from controller: WidgetController) {
		guard let data = viewInstructionsViewModel.data else {
			onComplete?()
			return
		}
		let url = FileManager.default.temporaryDirectory.appendingPathComponent("instructions").appendingPathExtension("pdf")
		do {
			try data.write(to: url)
			openURI(url.absoluteString, from: controller)
		} catch {
		}
		viewInstructionsViewModel.didComplete()
	}

}
