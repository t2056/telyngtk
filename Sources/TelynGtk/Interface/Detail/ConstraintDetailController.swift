import Foundation
import Gtk
import GtkKit
import OpenCombine
import TelynViewModel

public class ConstraintDetailController: SectionedListController<ConstraintDetailSection, ConstraintDetailItem> {

	public override var bundle: Bundle? {
		return Bundle.module
	}

	public override var widgetName: String {
		return "detail_widget"
	}

	public var viewModel = ConstraintDetailViewModel()

	public lazy var title: Label = child(named: "title")

	public lazy var deleteButton: Button = child(named: "delete_button")

	public lazy var renameButton: Button = child(named: "rename_button")

	var cancellables = Set<AnyCancellable>()

	var sectionCancellables = Set<AnyCancellable>()

	public override func widgetDidLoad() {
		viewModel.$sections.onMain { [unowned self] (sections) in
			setSections(to: sections)
			sectionCancellables = Set<AnyCancellable>()
			for section in sections {
				viewModel.items(for: section).onMain { [unowned self] (items) in
					setItems(to: items, in: section)
				}.store(in: &sectionCancellables)
			}
		}.store(in: &cancellables)
		viewModel.name.bind(to: title).store(in: &cancellables)
		renameButton.onClicked { [unowned self] (_) in
 			onRename()
		}
		deleteButton.onClicked { [unowned self] (_) in
			onDelete()
		}

		keyActions = [
			KeyAction(input: "r", modifierFlags: .control, action: onRename),
			KeyAction(input: "d", modifierFlags: .control, action: onDelete)
		]
	}

	public override func generateWidget(for item: ConstraintDetailItem) -> Widget {
		switch item {
			case .node(let node):
				let row: LinkRow = buildWidget(named: "link_row")
				node.publisher(for: \.name).bind(to: row.label).store(in: &row.cancellables)
				return row
			case .delegateType:
				let row: ComboBoxRow = buildWidget(named: "combo_box_row")
				row.label.text = viewModel.title(for: .delegateType)
				row.setSegments(to: viewModel.delgateTypeSegmentTitles)
				viewModel.delegateTypeIndex.onMain { (index) in
					row.selection = index
				}.store(in: &row.cancellables)
				row.onChanged { [unowned self] (index) in
					viewModel.selectDelegateType(at: index)
				}
				return row
			case .measurement:
				let row: ComboBoxRow = buildWidget(named: "combo_box_row")
				row.label.text = viewModel.title(for: .measurement)
				viewModel.availableMeasurementNames.sink() { [unowned row] (availableMeasurementNames) in
					row.elements = availableMeasurementNames
				}.store(in: &row.cancellables)
				viewModel.measurementIndex.sink() { [unowned row] (measurementIndex) in
					row.selection = measurementIndex
				}.store(in: &row.cancellables)
				row.comboBox.onChanged { [weak viewModel] (comboBox) in
					guard comboBox.active >= 0 else {
						return
					}
					viewModel?.selectMeasurementType(at: comboBox.active)
				}
				return row
			case .base:
				let row: NumberFieldRow = buildWidget(named: "number_field_row")
				row.label.text = viewModel.title(for: .base)
				row.spinbutton.setRange(min: -10_000, max: 10_000)
				row.spinbutton.setIncrements(step: 1, page: 1)
				row.spinbutton.digits = 1
				viewModel.base.onMain() { [weak row] (base) in
					row?.spinbutton.value = base
				}.store(in: &row.cancellables)
				row.onValueChanged { [unowned viewModel] (value) in
					viewModel.setBase(to: value)
				}
				return row
			case .allowance:
				let row: NumberFieldRow = buildWidget(named: "number_field_row")
				row.label.text = viewModel.title(for: .allowance)
				row.spinbutton.setRange(min: -10_000, max: 10_000)
				row.spinbutton.setIncrements(step: 1, page: 1)
				row.spinbutton.digits = 1
				viewModel.allowance.bindToValue(of: row.spinbutton).store(in: &row.cancellables)
				row.onValueChanged { [unowned viewModel] (value) in
					viewModel.setAllowance(to: value)
				}
				return row
		}
	}

	public override func title(for section: ConstraintDetailSection) -> String? {
		return viewModel.title(for: section)
	}

	public override func activate(in section: Int, at index: Int) {
		viewModel.selected(at: section, in: index)
	}

	public func onRename() {
		viewModel.rename()
	}

	public func onDelete() {
		viewModel.delete()
	}

}
