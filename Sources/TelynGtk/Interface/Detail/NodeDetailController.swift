import Foundation
import Gtk
import GtkKit
import OpenCombine
import TelynCore
import TelynViewModel

public class NodeDetailController: SectionedListController<NodeDetailSection, NodeDetailItem> {

	public override var bundle: Bundle? {
		return Bundle.module
	}

	public override var widgetName: String {
		return "detail_widget"
	}

	public var viewModel = NodeDetailViewModel()

	lazy var title: Label = child(named: "title")

	lazy var deleteButton: Button = child(named: "delete_button")

	lazy var renameButton: Button = child(named: "rename_button")

	var cancellables = Set<AnyCancellable>()

	var sectionCancellables = Set<AnyCancellable>()

	public override func widgetDidLoad() {
		viewModel.sections.onMain { [unowned self] (sections) in
			setSections(to: sections)
			sectionCancellables = Set<AnyCancellable>()
			for section in sections {
				viewModel.items(for: section).onMain { [unowned self] (items) in
					setItems(to: items, in: section)
				}.store(in: &sectionCancellables)
			}
		}.store(in: &cancellables)
		viewModel.name.bind(to: title).store(in: &cancellables)
		renameButton.onClicked { [unowned self] (_) in
			onRename()
		}
		deleteButton.onClicked { [unowned self] (_) in
			onDelete()
		}

		keyActions = [
			KeyAction(input: "r", modifierFlags: .control, action: onRename),
			KeyAction(input: "d", modifierFlags: .control, action: onDelete)
		]
	}

	public override func generateWidget(for item: NodeDetailItem) -> Widget {
		switch item {
			case .position(let axis):
				let row: NumberFieldRow = buildWidget(named: "number_field_row")
				row.label.text = viewModel.title(for: axis)
				row.spinbutton.digits = 1
				row.spinbutton.setIncrements(step: 1, page: 1)
				row.spinbutton.setRange(min: -10_000, max: 10_000)
				viewModel.position(for: axis).bindToValue(of: row.spinbutton).store(in: &row.cancellables)
				row.onValueChanged { [unowned self] (value) in
					viewModel.setPosition(to: value, on: axis)
				}
				return row
			case .constraint(let constraint):
				let row: LinkRow = buildWidget(named: "link_row")
				constraint.publisher(for: \.name).bind(to: row.label).store(in: &cancellables)
				return row
		}
	}

	public override func title(for section: NodeDetailSection) -> String? {
		switch section {
			case .position:
				return "Position"
			case .constraints:
				return "Constraints"
		}
	}

	public override func activate(in section: Int, at index: Int) {
		viewModel.selected(item: model.items(in: model.sections[section])[index])
	}

	public func onRename() {
		viewModel.rename()
	}

	public func onDelete() {
		viewModel.delete()
	}

}
