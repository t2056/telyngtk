import Foundation
import Gtk
import GtkKit
import OpenCombine
import TelynCore
import TelynViewModel

public class LineDetailController: SectionedListController<LineDetailSection, LineDetailItem> {

	public override var bundle: Bundle? {
		return Bundle.module
	}

	public override var widgetName: String {
		return "detail_widget"
	}

	public var viewModel = LineDetailViewModel()

	lazy var title: Label = child(named: "title")

	lazy var renameButton: Button = child(named: "rename_button")

	lazy var deleteButton: Button = child(named: "delete_button")

	var cancellables = Set<AnyCancellable>()

	var sectionCancellables = Set<AnyCancellable>()

	public override func widgetDidLoad() {
		viewModel.sections.sink { [unowned self] (sections) in
			setSections(to: sections)
			sectionCancellables = Set<AnyCancellable>()
			for section in sections {
				viewModel.items(for: section).sink { [unowned self] (items) in
					setItems(to: items, in: section)
				}.store(in: &sectionCancellables)
			}
		}.store(in: &cancellables)
		viewModel.name.bind(to: title).store(in: &cancellables)
		renameButton.onClicked { [unowned self] (_) in
			onRename()
		}
		deleteButton.onClicked { [unowned self] (_) in
			onDelete()
		}

		keyActions = [
			KeyAction(input: "r", modifierFlags: .control, action: onRename),
			KeyAction(input: "d", modifierFlags: .control, action: onDelete)
		]
	}

	public override func generateWidget(for item: LineDetailItem) -> Widget {
		switch item {
			case .node(let node):
				let row: LinkRow = buildWidget(named: "link_row")
				node.publisher(for: \.name).bind(to: row.label).store(in: &row.cancellables)
				return row
			case .isEdge:
				let row: ToggleRow = buildWidget(named: "toggle_row")
				row.label.text = viewModel.title(for: item)
				viewModel.isEdge.bind(to: row.toggle).store(in: &row.cancellables)
				row.toggle.onNotifyActive { [unowned self] (toggle, _) in
					viewModel.setIsEdge(to: toggle.active)
				}
				return row
			case .isHem:
				let row: ToggleRow = buildWidget(named: "toggle_row")
				row.label.text = viewModel.title(for: item)
				viewModel.isHem.bind(to: row.toggle).store(in: &row.cancellables)
				row.toggle.onNotifyActive { [unowned self] (toggle, _) in
 					viewModel.setIsHem(to: toggle.active)
				}
				return row
			case .numberOfControlPoints:
				let row: ComboBoxRow = buildWidget(named: "combo_box_row")
				row.label.text = viewModel.title(for: item)
				// The number of control points can act as index.
				row.setSegments(to: viewModel.controlPointTitles())
				viewModel.numberOfControlPoints.bindToActiveElement(of: row.comboBox).store(in: &cancellables)
				row.onChanged { [unowned self] (numberOfControlPoints) in
					viewModel.setNumberOfControlPoints(to: numberOfControlPoints)
					print(viewModel.line?.numberOfControlPoints)
					print(viewModel.line?.controlPoints)
				}
				return row
			case .controlPoint(let index, let axis):
				let row: NumberFieldRow = buildWidget(named: "number_field_row")
				row.label.text = viewModel.title(for: item)
				row.spinbutton.setRange(min: -10_000, max: 10_000)
				row.spinbutton.setIncrements(step: 1, page: 1)
				row.spinbutton.digits = 1
				viewModel.controlPoint(for: index).map { (controlPoint) -> Double in
					switch axis {
						case .x:
							return controlPoint.x
						case .y:
							return controlPoint.y
					}
				}.bindToValue(of: row.spinbutton).store(in: &row.cancellables)
				row.onValueChanged { [unowned self] (newValue) in
					viewModel.setControlPointPosition(to: newValue, on: axis, at: index)
				}
				return row
		}
	}

	public override func title(for section: LineDetailSection) -> String? {
		return viewModel.title(for: section)
	}

	public override func activate(in section: Int, at index: Int) {
		let item = model.items(in: model.sections[section])[index]
		viewModel.selected(item: item)
	}

	public func onRename() {
		viewModel.rename()
	}

	public func onDelete() {
		viewModel.delete()
	}

}
