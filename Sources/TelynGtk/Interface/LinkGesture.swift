import Foundation
import CGtk
import Gtk

/// LinkGesture is a bespoke custom gesture used for the gesture used for links in the component editor.
/// It activates when the user drags after a long press, while holding the right mouse button, or while holding control
public class LinkGesture {

	public var widget: Widget

	/// This gesture enables the gesture if the gesture begins with a long press.
	public var longPressGesture: GestureLongPress

	public var dragGesture: GestureDrag

	public var origin: CGPoint?

	public var longPressDetected: Bool = false

	public var active: Bool = false

	public var failed: Bool = false

	public var beginHandler: ((CGPoint) -> Bool)?

	public var updateHandler: ((CGPoint) -> Void)?

	public var endHandler: ((CGPoint) -> Void)?

	public var failedHandler: (() -> Void)?

	public init(widget: Widget) {
		self.widget = widget
		longPressGesture = GestureLongPress(widget: widget)
		longPressGesture.set(property: .delayFactor, value: 0.25)
		dragGesture = GestureDrag(widget: widget)
		dragGesture.button = 0
		gtk_gesture_group(longPressGesture.gesture_ptr, dragGesture.gesture_ptr)
		longPressGesture.onPressed(handler: onLongPress(gesture:x:y:))
		dragGesture.onDragBegin(handler: onDragBegin)
		dragGesture.onDragUpdate(handler: onDragUpdate)
		dragGesture.onDragEnd(handler: onDragEnd)
	}

	public func onBegin(_ handler: @escaping (CGPoint) -> Bool) {
		self.beginHandler = handler
	}

	public func onUpdate(_ handler: @escaping (CGPoint) -> Void) {
		self.updateHandler = handler
	}

	public func onEnd(_ handler: @escaping (CGPoint) -> Void) {
		self.endHandler = handler
	}

	public func onFailed(_ handler: @escaping () -> Void) {
		failedHandler = handler
	}

	public func onLongPress(gesture: GestureLongPressRef, x: Double, y: Double) {
		longPressDetected = true
	}

	public func onDragBegin(gesture: GestureDragRef, x: Double, y: Double) {
		origin = CGPoint(x: x, y: y)
		if isCtrlPressed(using: widget) || gesture.currentButton == 3 {
			begin()
		}
	}

	public func onDragUpdate(gesture: GestureDragRef, x: Double, y: Double) {
		if active, let point = position(xOffset: x, yOffset: y) {
			update(at: point)
		} else if longPressDetected {
			begin()
		} else if !failed {
			failed = true
		}
	}

	public func onDragEnd(gesture: GestureDragRef, x: Double, y: Double) {
		if active, let point = position(xOffset: x, yOffset: y) {
			end(at: point)
		}
		// Cleanup for the next gesture
		longPressDetected = false
		active = false
		failed = false
	}

	public func begin() {
		guard let beginHandler = beginHandler, let origin = origin else {
			return
		}
		active = beginHandler(origin)
		if active {
			dragGesture.set(state: .claimed)
		}
	}

	public func fail() {
		failed = true
		dragGesture.set(state: .denied)
		failedHandler?()
	}

	public func update(at point: CGPoint) {
		updateHandler?(point)
	}

	public func end(at point: CGPoint) {
		endHandler?(point)
	}

	public func position(xOffset: Double, yOffset: Double) -> CGPoint? {
		return origin?.shift(by: CGPoint(x: xOffset, y: yOffset))
	}

}
