import Foundation
import GtkKit
import TelynViewModel

public class TelynWindowDelegate: WindowDelegate, PresentationDelegate {

	var navigator: TelynNavigator?

	public func presentationWillBegin(_ presentationController: PresentationController) {
		print("presentationWillBegin(_ presentationController)")
		let componentSelectionController = ComponentSelectionController()
		let rootController = NavigationController()
		navigator = TelynNavigator(withRoot: rootController)
		navigator?.rootInstall(using: .componentSelection, style: .push)
		presentationController.install(controller: rootController)
	}

	public func presentationDidEnd(_ presentation: PresentationController) {
	            print("Main window was closed")
	            if let measurementProvider = measurementProvider as? MeasurementManager {
	            	measurementProvider.save()
	            }
	            if let blockProvider = blockProvider as? BlockManager {
	            	blockProvider.save()
	            }
	            if let controller = navigator?.rootViewController.children[0] as? ComponentSelectionController {
	            	controller.viewModel.save()
	            }
	}

}
