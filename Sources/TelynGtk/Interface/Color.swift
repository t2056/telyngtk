import Foundation
import GtkKit

/// These colors are taken from the png file 'adwaita-color-palette.png'. They are apparently part of a palette used for gtk themes, but I can't find any other reference to them. Regardless, they are attractive colors that fit in with adwaita, so it is worth defining them. Consider adding the rest of the colours, and integrating into GtkKit.
public extension Color {

	static let systemBlue1 = Color(red: 149.0 / 255.0, green: 191.0 / 255.0, blue:  238.0 / 255.0)

	static let systemBlue2 = Color(red: 93.0 / 255.0, green: 157.0 / 255.0, blue: 230.0 / 255.0)

	static let systemBlue3 = Color(red: 47.0 / 255.0, green: 129.0 / 255.0, blue: 224 / 255.0)

	static let systemBlue4 = Color(red: 18.0 / 255.0, green: 110.0 / 255.0, blue: 211.0 / 255.0 )
	
	static let systemBlue5 = Color(red: 22.0 / 255.0, green: 93.0 / 255.0, blue: 176.0 / 255.0)

}
