import Foundation
import Gtk
import Cairo
import GtkKit
import TelynCore

public class ComponentPreviewWidget: DrawingArea {

	public var preview: Preview? {
		didSet {
			queueDraw()
		}
	}

	public override init() {
		super.init()
		onDraw(handler: draw(in:with:))
	}

	public required init(raw: UnsafeMutableRawPointer) {
		super.init(raw: raw)
		onDraw(handler: draw(in:with:))
	}

	public required init(retainingRaw raw: UnsafeMutableRawPointer) {
		super.init(retainingRaw: raw)
		onDraw(handler: draw(in:with:))
	}

	public func draw(in widget: WidgetRef, with context: Cairo.ContextRef) -> Bool {
		guard let preview = preview, let start = preview.first else {
			return false
		}
		let width = allocatedWidth - marginStart - marginEnd
		let height = allocatedHeight - marginTop - marginBottom
		let size = CGSize(width: width, height: height)
		let origin = CGPoint(x: marginStart, y: marginEnd)
		let translate = { (point: RelativePoint) -> CGPoint in
			return point.asCG(in: size).shift(by: origin)
		}
		Color.systemBlue4.set(on: context)
		let path = BezierPath()
		path.move(to: translate(start.target))
		for component in preview[1...] {
			if component.controlPoints.isEmpty {
				path.addLine(to: translate(component.target))
			} else if component.controlPoints.count == 1 {
				path.addQuadCurve(to: translate(component.target), control: translate(component.controlPoints[0]))
			} else {
				path.addCurve(to: translate(component.target), controlA: translate(component.controlPoints[0]), controlB: translate(component.controlPoints[1]))
			}
		}
		path.close()
		path.lineWidth = 4
		path.lineJoinStyle = .round
		path.stroke(on: context)
		return false
	}

}
