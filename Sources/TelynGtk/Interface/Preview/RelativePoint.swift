import Foundation
import TelynCore

public extension RelativePoint {
	
	func asCG(in size: CGSize) -> CGPoint {
		return CGPoint(x: size.width * CGFloat(x), y: size.height * CGFloat(y))
	}
	
}

public extension CGPoint {
	
	func shift(by other: CGPoint) -> CGPoint {
		return CGPoint(x: x + other.x, y: y + other.y)
	}
	
}
