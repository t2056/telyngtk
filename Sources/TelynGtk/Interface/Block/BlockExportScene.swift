import Foundation
import GtkKit
import TelynCore
import TelynViewModel

public class BlockExportScene: GtkNavigatorCustomScene {

	public var blockExportViewModel = BlockExportViewModel()

	public init() {
		super.init(viewModel: blockExportViewModel)
	}

	public override func present(from controller: WidgetController) {
		guard let data = blockExportViewModel.getData() else {
			blockExportViewModel.onError()
			return
		}
		let documentPicker = DocumentPickerController.forSavingFile(ofType: .blocks, title: "Export to Blocks", onFileSelected: { [blockExportViewModel] (url) in
			do {
				try FileManager.default.createFile(atPath: url.path, contents: nil)
				try data.write(to: url)
				blockExportViewModel.didComplete()
			} catch {
				blockExportViewModel.onError()
			}
		})
		documentPicker.confirmButtonTitle = "Export"
		documentPicker.onCancel = { [blockExportViewModel] () in
			blockExportViewModel.didComplete()
		}
		controller.present(documentPicker)
	}

}
