import Foundation
import Gtk
import GtkKit
import TelynCore
import TelynViewModel
import OpenCombine

public class BlockImportController: CollectionWidgetController<BlockImportSection, BlockImportItem> {

	public override var bundle: Bundle? {
		return Bundle.module
	}

	var viewModel = BlockImportViewModel()

	var cancellables = Set<AnyCancellable>()

	var sectionCancellables = Set<AnyCancellable>()

	public override func widgetDidLoad() {
		viewModel.sections.onMain() { [unowned self] (sections) in
			setSections(to: sections)
			sectionCancellables = []
			for section in sections {
				viewModel.items(for: section).onMain() { [unowned self] (items) in
					setItems(to: items, in: section)
				}.store(in: &cancellables)
			}
		}.store(in: &cancellables)
		headerbarItem.startItems = [
			BarButtonItem(title: "Close", onClick: onClose(_:))
		]
		headerbarItem.title = "Add a Component from Blocks"
		collectionWidget.setPlaceholder(to: Label(text: "No Blocks Available"))
	}

	public override func generateWidget(for item: BlockImportItem) -> Widget {
		let cell: ComponentPreviewCell = buildWidget(named: "component_preview_cell")
		switch item {
			case .compatible(let block):
				cell.preview.preview = block.preview
				cell.name.text = block.name
			case .incompatible(let block):
				cell.preview.preview = block.preview
				cell.name.text = block.name
				cell.warningImage.visible = true
		}
		return cell
	}

	public override func generateLayout(for section: BlockImportSection) -> CollectionLayoutSection {
		return CollectionLayoutFlowSection(rowSpacing: 8, columnSpacing: 8, orientation: .horizontal, minChildren: 1, maxChildren: 4, homogenous: true)
	}

	public override func generateHeader(for section: BlockImportSection) -> Widget? {
		let header: SimpleTitleHeader = buildWidget(named: "simple_title_header")
		header.titleLabel.text = viewModel.title(for: section)
		return header
	}

	public override func activate(in section: BlockImportSection, for item: BlockImportItem) {
		viewModel.selected(item: item)
	}

	public func onClose(_ button: ButtonProtocol) {
		viewModel.close()
	}

}
