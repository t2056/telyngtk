import Foundation
import Gtk
import GtkKit
import TelynCore
import TelynViewModel
import OpenCombine

public class BlockLibraryController: CollectionWidgetController<SingleSection, Block> {

	public override var bundle: Bundle? {
		return Bundle.module
	}

	public var viewModel = BlockLibraryViewModel()

	var cancellables = Set<AnyCancellable>()

	public override func widgetDidLoad() {
		headerbarItem.startItems = [
			BarButtonItem(title: "Close", onClick: onClose)
		]
		headerbarItem.title = "Blocks"
		headerbarItem.endItems = [
			BarButtonItem(title: "Import", onClick: onImportBlocks)
		]
		setSections(to: [.only])
		viewModel.blocks.onMain() { [unowned self] (blocks) in
			setItems(to: blocks, in: .only)
		}.store(in: &cancellables)
		collectionWidget.setPlaceholder(to: Label(text: "No Blocks Imported"))

		keyActions = [
			KeyAction(input: "i", modifierFlags: .control, action: onImportBlocks)
		]
	}

	public override func generateWidget(for item: Block) -> Widget {
		let cell: ComponentPreviewCell = buildWidget(named: "component_preview_cell")
		cell.name.text = item.name
		cell.preview.preview = item.preview
		cell.contextActivateHandler = {
			let menu = ActionMenu(children: [
				Action(title: "Remove", handler: { [weak self] () in
					self?.viewModel.delete(block: item)
				})
			])
			menu.present(from: cell)
		}
		return cell
	}

	public override func generateLayout(for section: SingleSection) -> CollectionLayoutSection {
		return CollectionLayoutFlowSection(rowSpacing: 8, columnSpacing: 8, orientation: .horizontal, minChildren: 1, maxChildren: 4, homogenous: true)
	}

	public func onImportBlocks() {
		present(DocumentPickerController.forOpeningFile(ofType: .blocks, title: "Import Blocks", onFileSelected: importBlocks(_:)))
	}

	public func importBlocks(_ url: URL) {
		if let data = try? Data(contentsOf: url), let collection = try? BlockCollection.decode(from: data) {
			blockProvider?.database?.add(blocks: collection)
		} else {
			let alert = AlertController(title: "Could Not Open File", message: nil)
			alert.addAction(AlertAction(title: "OK"))
			present(alert)
		}
	}

	func onClose() {
		viewModel.close()
	}

}
