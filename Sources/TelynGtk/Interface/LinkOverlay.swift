import Foundation
import Gtk
import Cairo
import GtkKit

let linkCircleRadius = 16.0

let linkAnimationTime = 0.5

let linkWidth = 4.0

public class LinkOverlay: DrawingArea {

	public var origin: CGPoint?

	public var target: CGPoint?

	public var startTime: Date = Date()

	public var canLink: Bool = false {
		didSet {
			if canLink, !oldValue {
				canLinkTime = Date()
			}
		}
	}

	// The time at which canLink last switched from false to true
	public var canLinkTime: Date = Date()

	public var renderClockId: Int?

	public override init() {
		super.init()
		becomeSwiftObj()
		onDraw(handler: draw)
	}

	public required init(raw: UnsafeMutableRawPointer) {
		super.init(raw: raw)
		becomeSwiftObj()
		onDraw(handler: draw)
	}

	public required init(retainingRaw raw: UnsafeMutableRawPointer) {
		super.init(retainingRaw: raw)
		becomeSwiftObj()
		onDraw(handler: draw)
	}

	public func beginLink(at point: CGPoint) {
		origin = point
		target = point
		canLink = false
		startClock()
	}

	public func updateLink(at point: CGPoint, canLink: Bool) {
		target = point
		self.canLink = canLink
	}

	public func endLink() {
		origin = nil
		target = nil
		canLink = false
		endClock()
		queueDraw()
	}

	public func startClock() {
		renderClockId = addTickCallback { [weak self] (_) in
			self?.queueDraw()
			return true
		}
	}

	public func endClock() {
		guard renderClockId != nil else {
			return
		}
		removeTickCallback(id: renderClockId!)
		renderClockId = nil
	}

	public func draw(_ widget: WidgetRef, in context: ContextRef) -> Bool{
		guard let origin = origin, let target = target else {
			return false
		}
		Color.systemBlue3.set(on: context)

		let line = BezierPath()
		line.move(to: origin)
		line.addLine(to: target)
		line.lineWidth = linkWidth
		line.lineCapStyle = .round
		line.stroke(on: context)

		drawLinkCircle(at: origin, startTime: startTime, in: context)

		if canLink {
			drawLinkCircle(at: target, startTime: canLinkTime, in: context)
 		}

 		return false

	}

	public func drawLinkCircle(at point: CGPoint, startTime: Date, in context: ContextRef) {
		let circle = BezierPath()
		let animationElapsedTime = Date().timeIntervalSince(startTime)
		let animationProgress = (animationElapsedTime / linkAnimationTime).ceiling(1.0)
		circle.addArc(withCenter: point, radius: linkCircleRadius * animationProgress, startAngle: 0, endAngle: Double.pi * 2, clockwise: true)
		circle.lineWidth = linkWidth
		circle.stroke(on: context)
	}


}
