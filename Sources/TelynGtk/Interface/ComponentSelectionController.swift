import Foundation
import Gtk
import GtkKit
import TelynCore
import TelynViewModel
import OpenCombine

public class ComponentSelectionController: CollectionWidgetController<SingleSection, ComponentSelectionItem> {

	public override var bundle: Bundle? {
		return Bundle.module
	}

	public var viewModel = ComponentSelectionViewModel()

	lazy var addComponentItem = BarButtonItem(iconName: "list-add-symbolic", menu: generateComponentCreationMenu())

	lazy var appMenuItem = BarButtonItem(iconName: "open-menu-symbolic")

	lazy var selectMeasurementsItem = BarButtonItem(iconName: "ruler-symbolic", onClick: onSelectMeasurements)

	lazy var printItem = BarButtonItem(iconName: "document-print-symbolic", onClick: onPrint)

	lazy var placeholderWidget = Label(text: "")

	var cancellables = Set<AnyCancellable>()

	public override func widgetDidLoad() {
		viewModel.showsAddCell = false
	    headerbarItem.leftItems = [
	    	BarButtonItem(iconName: "document-open-symbolic", onClick: onOpenDocument),
	    	BarButtonItem(iconName: "document-new-symbolic", onClick: onCreateDocument),
	    ]
	    headerbarItem.rightItems = [
	    	appMenuItem,
	    	addComponentItem,
	    	printItem,
	    	selectMeasurementsItem
	    ]
	    viewModel.title.map { (title) -> String? in
	    	return title ?? "No Pattern Open"
	    }.eraseToAnyPublisher().bindAsTitle(of: headerbarItem).store(in: &cancellables)
	    headerbarItem.title = "Test"
	    setSections(to: [.only])
	    viewModel.items.onMain { [unowned self] (items) in
	    	setItems(to: items, in: .only)
	    }.store(in: &cancellables)

	    viewModel.hasPattern.onMain { [unowned self] (hasPattern) in
	    	print("hasPattern: \(hasPattern)")
	    	addComponentItem.active = hasPattern
	    	selectMeasurementsItem.active = hasPattern
	    	printItem.active = hasPattern
	    	placeholderWidget.text = hasPattern ? "No Components": ""
	    }.store(in: &cancellables)
	    viewModel.$menuState.onMain() { [unowned self] (menuState) in
	    	print("Invoked menuState subscription with value: \(menuState)")
	    	buildAppMenu(menuState: menuState)
	    }.store(in: &cancellables)
		collectionWidget.setPlaceholder(to: Label(text: "No Components"))

		keyActions = [
			KeyAction(input: "o", modifierFlags: .control, action: onOpenDocument),
			KeyAction(input: "n", modifierFlags: .control, action: onCreateDocument),
			KeyAction(input: "a", modifierFlags: .control, action: onAddBlankComponent),
			KeyAction(input: "a", modifierFlags: [.control, .shift], action: onImportComponentFromBlock),
			KeyAction(input: "m", modifierFlags: .control, action: onSelectMeasurements),
			KeyAction(input: "m", modifierFlags: [.control, .shift], action: onOpenMeasurementManager),
			KeyAction(input: "b", modifierFlags: .control, action: onOpenBlockManager),
			KeyAction(input: "p", modifierFlags: .control, action: onPrint),
			KeyAction(input: "i", modifierFlags: .control, action: onViewInstructions),
			KeyAction(input: "i", modifierFlags: [.control, .shift], action: onAddInstructions),
			KeyAction(input: "h", modifierFlags: .control, action: onOpenManual)
		]
	}

	public override func generateWidget(for item: ComponentSelectionItem)-> Widget {
		switch item {
			case .component(let component):
				let cell: ComponentPreviewCell = buildWidget(named: "component_preview_cell")
				component.publisher(for: \.name).onMain { [unowned cell] (output) in
					cell.name.label = output
				}.store(in: &cell.cancellables)
				component.publisher(for: \.preview).onMain { [unowned cell] (preview) in
					cell.preview.preview = preview
				}.store(in: &cell.cancellables)
				cell.contextActivateHandler = { [weak self, weak component, unowned cell] () in
					guard let strongSelf = self, let component = component else {
						return
					}
					strongSelf.onShowContextMenu(from: cell, for: component)
				}
				return cell
			default:
				return Label(text: "Error!")
		}
	}

	public func onOpenDocument() {
		present(DocumentPickerController.forOpeningFile(ofType: .pattern, title: "Open a Pattern", onFileSelected: openDocument(_:)))
	}

	public func openDocument(_ url: URL) {
		if let document = PatternDocument(fromFileAt: url.path) {
			viewModel.patternProvider = document
		} else {
			let alert = AlertController(title: "Could not Open File", message: nil)
			alert.addAction(AlertAction(title: "OK"))
			present(alert)
		}
	}

	public func onCreateDocument() {
		let documentPicker = DocumentPickerController.forCreatingFile(ofType: .pattern, title: "Create a New Pattern", onFileSelected: createPattern(_:))
		documentPicker.confirmButtonTitle = "Create"
		present(documentPicker)
	}

	public func createPattern(_ url: URL) {
		let name = url.lastPathComponent
		viewModel.patternProvider = PatternDocument(newNamed: name, at: url)
	}

	public func onAddBlankComponent() {
		print("Do things")
		viewModel.addComponent()
	}

	public func onImportComponentFromBlock() {
		viewModel.importBlock()
	}

	public func onSelectMeasurements() {
		viewModel.selectMeasurement()
	}

	public func onPrint() {
		viewModel.onPrint()
	}

	public func onExportAsBlocks() {
		viewModel.exportBlocks()
	}

	public func onAddInstructions() {
		viewModel.addInstructions()
	}

	public func onViewInstructions() {
		viewModel.viewInstructions()
	}

	public func onOpenMeasurementManager() {
		viewModel.openMeasurementEditor()
	}

	public func onOpenBlockManager() {
		viewModel.openBlockLibrary()
	}

	public func onOpenManual() {
		openManual(from: self)
	}

	public func generateComponentCreationMenu() -> ActionMenu {
		return ActionMenu(children: [
			Action(title: "From Blank", handler: onAddBlankComponent),
			Action(title: "From Block", handler: onImportComponentFromBlock)
		])
	}

	public func buildAppMenu(menuState: (hasPattern: Bool, hasInstructions: Bool)) {
		var actions: [GtkKit.MenuElement] = []
		if menuState.hasPattern {
			actions.append(contentsOf: [
				Action(title: "Export As Blocks", handler: onExportAsBlocks),
				MenuSeparator(),
				Action(title: "Attach Instructions", handler: onAddInstructions)
			] as [GtkKit.MenuElement])
			if menuState.hasInstructions {
				actions.append(Action(title: "View Instructions", handler: onViewInstructions))
			}
			actions.append(MenuSeparator())
		}
		actions.append(contentsOf: [
			Action(title: "Manage Measurements", handler: onOpenMeasurementManager),
			Action(title: "Manage Blocks", handler: onOpenBlockManager),
			MenuSeparator(),
			Action(title: "Help", handler: onOpenManual)
		] as [GtkKit.MenuElement])
		appMenuItem.menu = ActionMenu(children: actions)
	}

	public override func activate(in section: Int, at index: Int) {
		viewModel.select(at: index)
	}

	public func onShowContextMenu(from cell: ComponentPreviewCell, for component: Component) {
		let menu = ActionMenu(children: [
			Action(title: "Rename") { [weak viewModel] in
				viewModel?.renameComponent(component)
			},
			Action(title: "Remove") { [weak viewModel] in
				viewModel?.removeComponent(component)
			}
		])
		menu.present(from: cell)
	}


	public override func generateLayout(for section: SingleSection) -> CollectionLayoutSection {
		return CollectionLayoutFlowSection(rowSpacing: 8, columnSpacing: 8, orientation: .horizontal, minChildren: 1, maxChildren: 6, homogenous: true)
	}

}

public class ComponentPreviewCell: AspectFrame {

	var cancellables = Set<AnyCancellable>()

	lazy var name: Label = child(named: "name_label")

	lazy var warningImage: Image = child(named: "warning_image")

	lazy var preview: ComponentPreviewWidget = child(named: "preview")

	var gestureRecogniser: GestureLongPress?

	var contextActivateHandler: (() -> Void)?

	public required init(raw: UnsafeMutableRawPointer) {
		super.init(raw: raw)
		becomeSwiftObj()
		setupEventHandling()
	}

	public required init(retainingRaw raw: UnsafeMutableRawPointer) {
		super.init(raw: raw)
		becomeSwiftObj()
		setupEventHandling()
	}

	public func setupEventHandling() {
		onButtonPressEvent { [unowned self] (widget, event) -> Bool in
			if event.button == 3 {
				contextActivateHandler?()
				return true
			} else {
				return false
			}
		}
		gestureRecogniser = GestureLongPress(widget: self)
		gestureRecogniser?.onPressed { [unowned self] (_, _, _) in
			contextActivateHandler?()
		}
	}

}
