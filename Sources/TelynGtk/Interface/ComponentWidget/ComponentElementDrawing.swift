import Foundation
import Gtk
import GtkKit
import Cairo
import TelynCore

extension ComponentWidget {

	func setColor(for element: Any, on context: ContextProtocol) {
		let level: SelectionLevel;
		if let element = element as? Node {
			level = selectionLevel(for: element);
		} else if let element = element as? Line {
			level = selectionLevel(for: element);
		} else if let element = element as? Constraint {
			level = selectionLevel(for: element);
		} else {
			level = .unselected;
		}
		setColor(for: level, on: context)
	}

	func setColor(for level: SelectionLevel, on context: Cairo.ContextProtocol) {
		switch level {
		case .primary:
			Color.systemBlue4.set(on: context)
		case .secondary:
			Color.systemBlue2.set(on: context);
		case .unselected:
			if self.isDarkTheme() {
				Color.systemNeutral1.set(on: context)
			} else {
				Color.systemNeutral8.set(on: context)
			}
		}
	}

	func selectionLevel(for node: Node) -> SelectionLevel {
		if let selected = selected as? Node {
			if selected == node {
				return .primary;
			}
			if selected.isConstrained(to: node) {
				return .secondary;
			}
		}
		return .unselected;
	}

	func selectionLevel(for line: Line) -> SelectionLevel {
		if let selected = selected as? Line, line == selected {
			return .primary;
		}
		if let selected = selected as? Node, line.contains(selected) {
			return.secondary;
		}
		return .unselected;
	}

	func selectionLevel(for constraint: Constraint) -> SelectionLevel {
		if let selected = selected as? Constraint, selected == constraint {
			return .primary
		}
		if let selected = selected as? Node, constraint.constrains(node: selected) {
			return .secondary;
		}
		// This shouldn't occur, as constraints are only displayed if they have at least secondary selection
		return .unselected;
	}

	public func drawElements(on context: Cairo.ContextProtocol) {
		guard let component = component else {
			return;
		}
		fillComponent(on: context);
		for line in component.lines {
			setColor(for: line, on: context)
			draw(line: line, on: context)
		}
		for node in component.nodes {
			setColor(for: node, on: context)
			draw(node: node, on: context)
		}
		var drawnConstraints: [Constraint] = []
		var primaryNode: Node? = nil
		if let selected = selected as? Node {
			drawnConstraints = selected.constraints
			primaryNode = selected
		}
		if let selected = selected as? Constraint {
			drawnConstraints = [selected]
		}
		let tracker = LayerTracker();
		for constraint in drawnConstraints {
			setColor(for: constraint, on: context)
			draw(constraint: constraint, primaryNode: primaryNode ?? constraint.nodeA, tracker: tracker, on: context);
		}
	}

	func fillComponent(on context: Cairo.ContextProtocol) {
		guard let component = component else {
			return
		}
		Color.systemNeutral4.set(on: context)
		for loop in component.loops {
			let path = BezierPath()
			path.move(to: renderContext.cgPoint(for: loop[0].node.position))
			for index in 1..<loop.count {
				let (node, line) = loop[index];
				if let line = line {
					if line.controlPoints.count == 0 {
						path.addLine(to: renderContext.cgPoint(for: node.position))
					} else if line.controlPoints.count == 1 {
						path.addQuadCurve(to: renderContext.cgPoint(for: node.position), control: renderContext.cgPoint(for: line.controlPoints[0]))
					} else {
						/// Because loops are searched for in one direction, the order of control points for the loop may not align with the order in the line, requiring flipping in that instance.
						let aIndex: Int
						let bIndex: Int
						if node == line.nodeB {
							aIndex = 0
							bIndex = 1
						} else {
							aIndex = 1
							bIndex = 0
						}
						path.addCurve(to: renderContext.cgPoint(for: node.position), controlA: renderContext.cgPoint(for: line.controlPoints[aIndex]), controlB: renderContext.cgPoint(for: line.controlPoints[bIndex]))
					}
				}
			}
			path.close()
			path.fill(on: context)
		}
	}

	func draw(node: Node, on context: Cairo.ContextProtocol) {
		let point = renderContext.cgPoint(for: node.position);
		// Draw the node as a full circle.
		let path = BezierPath()
		path.addArc(withCenter: renderContext.cgPoint(for: node.position), radius: 16, startAngle: 0, endAngle: 2 * Double.pi, clockwise: true)
		path.fill(on: context)
		// Register the touch location for this node.
		path.lineWidth = touchZoneStrokeWidth
		self.touchLocations.insert((path, node), at: 0)
	}

	public func draw(line: Line, on context: Cairo.ContextProtocol) {
		let path = BezierPath()
		path.lineWidth = 8
		if !line.isEdge {
			path.setLineDash([16, 16], phase: 0)
		}
		path.move(to: renderContext.cgPoint(for: line.nodeA.position))
		if line.numberOfControlPoints == 0 {
			path.addLine(to: renderContext.cgPoint(for: line.nodeB.position))
			path.stroke(on: context)
		} else if line.numberOfControlPoints == 1 {


			path.addQuadCurve(to: renderContext.cgPoint(for: line.nodeB.position), control: renderContext.cgPoint(for: line.controlPoints[0]))
			path.stroke(on: context)

			if let selected = selected as? Line, selected == line {

				Color.systemBlue2.set(on: context)
				// Draw the control point.
				let origin = renderContext.cgPoint(for: line.controlPoints[0]);
				let cPath = BezierPath()
				cPath.addArc(withCenter: origin, radius: 8, startAngle: 0, endAngle: 2 * Double.pi, clockwise: true)
				cPath.fill(on: context);
				cPath.lineWidth = touchZoneStrokeWidth
				// Register the touch locations
				self.lineControlPointTouchLocations = [cPath]
			}
		} else {

			path.addCurve(to: renderContext.cgPoint(for: line.nodeB.position), controlA: renderContext.cgPoint(for: line.controlPoints[0]), controlB: renderContext.cgPoint(for: line.controlPoints[1]))
			path.stroke(on: context)

			if let selected = selected as? Line, selected == line {

				let cPoint1 = renderContext.cgPoint(for: line.controlPoints[0])
				let cPoint2 = renderContext.cgPoint(for: line.controlPoints[1])

				// Draw lines from the origin to the first control point and the second control point to the target to indicate order.

				Color.systemNeutral6.set(on: context)
				let line1 = BezierPath()
				line1.move(to: renderContext.cgPoint(for: line.nodeA.position))
				line1.addLine(to: cPoint1)
				line1.lineWidth = 1
				line1.stroke(on: context)
				let line2 = BezierPath()
				line2.move(to: renderContext.cgPoint(for: line.nodeB.position))
				line2.addLine(to: cPoint2)
				line2.lineWidth = 1
				line2.stroke(on: context)

				Color.systemBlue2.set(on: context)
				// Draw the control point indicators
				let cPath1 = BezierPath()
				cPath1.addArc(withCenter: cPoint1, radius: 8, startAngle: 0, endAngle: 2 * Double.pi, clockwise: true)
				cPath1.fill(on: context)
				cPath1.lineWidth = touchZoneStrokeWidth
				let cPath2 = BezierPath()
				cPath2.addArc(withCenter: cPoint2, radius: 8, startAngle: 0, endAngle: 2 * Double.pi, clockwise: true)
				cPath2.fill(on: context)
				cPath2.lineWidth = touchZoneStrokeWidth
				// Register the touch locations
				lineControlPointTouchLocations = [cPath1, cPath2]
			}
		}
		path.lineWidth = touchZoneStrokeWidth
		path.setLineDash(nil, phase: 0)
		// Register the touch location for this line. Make the lineWidth larger so the line is easier to hit.
		self.touchLocations.insert((path, line), at: 0)
	}

	public func draw(constraint: Constraint, primaryNode: Node, tracker: LayerTracker, on context: Cairo.ContextProtocol) {
		let secondaryNode = otherNode(than: primaryNode, in: constraint)!;
		// Perform universal setup.
		var cursor = renderContext.cgPoint(for: primaryNode.position);
		let path = BezierPath();
		path.lineWidth = 8;
		path.lineCapStyle = .round;
		path.lineJoinStyle = .round;
		// Calculate the location.
		let location = constraintLocation(from: primaryNode, to: secondaryNode, on: constraint.axis);
		let delta = self.delta(for: location);
		let layer = tracker[location] ?? 0;
		switch constraint.axis {
		case .x:
			// Move the cursor by the delta * 2 * layer + 1 on the y axis to prevent node overlap and implement layering.
			cursor.y = cursor.y + (delta * CGFloat(2 * (layer + 1)));
			// Begin constructing the path.
			path.move(to: cursor);
			// Move the cursor by the delta to give the bar effect.
			cursor.y = cursor.y + delta;
			path.addLine(to: cursor);
			// Move the cursor to x location of the other node.
			cursor.x = renderContext.cgPoint(for: secondaryNode.position).x;
			path.addLine(to: cursor);
			// Move the cursor by negative delta to complete the bar effect.
			cursor.y = cursor.y - delta;
			path.addLine(to: cursor);
		case .y:
			var cursor = renderContext.cgPoint(for: primaryNode.position);
			// Move the cursor by the delta * 2 * layer + 1 on the x axis to prevent node overlap and implement layering.
			cursor.x = cursor.x + (delta * CGFloat(2 * (layer + 1)));
			path.move(to: cursor);
			// Move the cursor by the delta for the bar effect.
			cursor.x = cursor.x + delta;
			path.addLine(to: cursor);
			// Move the cursor's y coordinate to the otherNode.
			cursor.y = renderContext.cgPoint(for: secondaryNode.position).y;
			path.addLine(to: cursor);
			// Move the cursor by the negative delta to complete the bar effect.
			cursor.x = cursor.x - delta;
			path.addLine(to: cursor);
		}
		path.stroke(on: context);
		// Register the touch locations for this constraint.
		path.lineWidth = touchZoneStrokeWidth
		self.touchLocations.insert((path, constraint), at: 0);
	}

	public func delta(for location: ConstraintLocation) -> CGFloat {
		switch location {
		case .above, .left:
			return -16;
		case .below, .right:
			return 16;
		}
	}

	public func constraintLocation(from selectedNode: Node, to otherNode: Node, on axis: Axis) -> ConstraintLocation {
		switch axis {
		case .x:
			if selectedNode.y <= otherNode.y {
				return .above;
			} else {
				return .below;
			}
		case .y:
			if selectedNode.x <= otherNode.x {
				return .left;
			} else {
				return.right;
			}
		}
	}

}

func otherNode(than node: Node, in constraint: Constraint) -> Node? {
	if constraint.nodeA == node {
		return constraint.nodeB;
	} else {
		return constraint.nodeA;
	}
}

// Represents where the
public enum ConstraintLocation {
	case above, below, left, right;
}

public class LayerTracker {

	private var map: [ConstraintLocation: Int];

	// Get the current layer for the specified location and increment it.
	public subscript(_ location: ConstraintLocation) -> Int {
		get {
			let layer = map[location]!;
			map[location] = layer + 1;
			return layer;
		}
	}

	public init() {
		map = [ConstraintLocation: Int]();
		map[.above] = 0;
		map[.below] = 0;
		map[.left] = 0;
		map[.right] = 0;
	}

}

public enum SelectionLevel {
	case primary
	case secondary
	case unselected
}
