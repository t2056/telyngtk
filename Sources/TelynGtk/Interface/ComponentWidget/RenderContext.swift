import Foundation
import TelynCore

public class RenderContext {

	public static let defaultScale = 5.0;

	// the upper-left corner of the rendered area of the pattern.
	public var origin: RealPoint;

	// The origin at the start of the pan.
	public var panBase: RealPoint?;

	// CGDistance = RealDistance * scale. RealDistance = CGDistance / scale.
	public var scale: Double;

	public var scaleBase: Double?;

	public var frame: CGRect {
		didSet {
			if !frame.isEmpty, let component = delayedFitComponent {
				adjustToFit(component: component);
				delayedFitComponent = nil;
			}
		}
	}

	public var zoomCentre: RealPoint?;

	public var delayedFitComponent: Component?;

	// Creates a new render context that shows the entirety of the component.
	public init(in frame: CGRect) {
		self.scale = RenderContext.defaultScale;
		self.frame = frame;
		// Default to reasonable values.
		origin = RealPoint(x: -20, y: -20);
	}

	public func adjustToFit(component: Component) {
		guard !frame.isEmpty else {
			delayedFitComponent = component;
			return
		}
		// Set origin to the upperLeft so we can call realDistance(for:) later.
		self.origin = component.frame.origin;
		if component.frame.size.width == 0 && component.frame.size.height == 0 {
			scale = 5
		}
		// If the component has a non-zero height or width, calculate the scale needed to fit the entirety of the component into the frame with 5% margins on either side of each axis, and set it to scale if it less than the current scale so that the component fits the entire component.
		if component.frame.size.width > 0 {
			let widthScale = Double(frame.width * 0.8) / component.frame.size.width;
			scale = widthScale
		}
		if component.frame.size.height > 0 {
			let heightScale = Double(frame.height * 0.8) / component.frame.size.height;
			if heightScale < scale {
				scale = heightScale;
			}
		}
		let center = component.center
		origin = center - RealPoint(x: realDistance(for: frame.width / 2), y: realDistance(for: frame.height / 2))
		// Adjust the origin to center the component
		print("center: \(center)")
		print(scale)
		print(origin)
	}

	public func realDistance(for cgDistance: CGFloat) -> Double {
		return Double(cgDistance) / scale;
	}

	public func cgDistance(for realDistance: Double) -> CGFloat {
		return CGFloat(realDistance * self.scale);
	}

	public func cgPoint(for realPoint: RealPoint) -> CGPoint {
		// Calculate the distance from the origin.
		let distanceFromOrigin = realPoint - origin;
		// Translate this distance into cgDistances to create the CGPoint
		return CGPoint(x: cgDistance(for: distanceFromOrigin.x), y: cgDistance(for: distanceFromOrigin.y));
	}

	public func realPoint(for cgPoint: CGPoint) -> RealPoint {
		let distanceFromOrigin = RealPoint(x: Double(cgPoint.x) / scale, y: Double(cgPoint.y) / scale);
		return distanceFromOrigin + origin;
	}

	public func startPan() {
		self.panBase = origin;
	}

	public func panBy(x cgx: CGFloat, y cgy: CGFloat) {
		if let panBase = panBase {
			let realX = -realDistance(for: cgx);
			let realY = -realDistance(for: cgy);
			self.origin = panBase + RealPoint(x: realX, y: realY);
		}
	}

	public func endPan() {
		self.panBase = nil;
	}

	public func startZoom() {
		self.scaleBase = self.scale;
		self.zoomCentre = origin + RealPoint(x: realDistance(for: frame.width * 0.5), y: realDistance(for: frame.height * 0.5));
	}

	public func zoom(by factor: Double) {
		if let scaleBase = scaleBase, let zoomCentre = zoomCentre {
			// Calculate the realPoint at the center of the current frame.
			self.scale = (scaleBase * factor).clamp(between: 1, and: 60);
			self.origin = zoomCentre - RealPoint(x: realDistance(for: frame.width * 0.5), y: realDistance(for: frame.height * 0.5));
		}
	}

	public func endZoom() {
		scaleBase = nil;
		zoomCentre = nil;
	}

	public func snappedPosition(for point: CGPoint) -> RealPoint {
		let output = realPoint(for: point)
		var snapInterval: Double
		if minorPointOpacity() > 0.9 {
			snapInterval = 1
		} else if displaysMinorPoints() {
			snapInterval = minorPointInterval
		} else {
			snapInterval = gridLineInterval()
		}
		return RealPoint(x: snap(output.x, toMultipleOf: snapInterval), y: snap(output.y, toMultipleOf: snapInterval))
	}

	public func snap(_ value: Double, toMultipleOf interval: Double) -> Double {
		let nearest = value.round(toNearestMultipleOf: interval)
		// The graphical distance between the actual position and the nearest integer
		let difference = (value - nearest).magnitude
		let graphicDistance = cgDistance(for: difference)
		if graphicDistance <= 8 {
			return nearest
		} else {
			// Cap precision in visual editing to multiples of one millimeter for usability
			return value.round(toNearestMultipleOf: 0.1)
		}
	}

	public func transitionToBring(rect: RealRect, into frame: CGRect) -> AnimatedTransition {
		// Calculate the scale needed to accomodate the entire rect into the frame
		// The maximum scale in which the rect can fit into the given frame.
		var targetScale = scale
		var widthScale = Double(frame.width) / rect.size.width
		// If the current scale exceeds the fitting scale, zoom out
		if widthScale < targetScale {
			targetScale = widthScale
		}
		// Repeat for height
		var heightScale = Double(frame.height) / rect.size.height
		if heightScale < targetScale {
			targetScale = heightScale
		}
		// Calculate the center, which is maintained in the zoom operation.
		let center = origin + RealPoint(x: realDistance(for: frame.width / 2), y: realDistance(for: frame.height / 2))
		// Calculate the origin after the zoom
		let targetRealDistance = { (cgDistance: CGFloat) in
			return Double(cgDistance) / targetScale
		}
		let newOrigin = center - RealPoint(x: targetRealDistance(frame.width / 2), y: targetRealDistance(frame.height / 2))
		// Calculate the rect after the zoom
		let targetFrameOrigin = newOrigin + RealPoint(x: targetRealDistance(frame.origin.x), y: targetRealDistance(frame.origin.y))
		let targetFrameSize = RealSize(width: targetRealDistance(frame.width), height: targetRealDistance(frame.height))
		let targetFrame = RealRect(origin: targetFrameOrigin, size: targetFrameSize)
		// Calculate the needed delta, in terms of movement of the origin, to bring the bounds into the frame. This is inverese to the movement of the bounds
		var xDelta: Double = 0
		var yDelta: Double = 0
		if rect.minX < targetFrame.minX {
			xDelta = rect.minX - targetFrame.minX
		}
		if rect.minY < targetFrame.minY {
			yDelta = rect.minY - targetFrame.minY
		}
		// Since the rect is no larget than the frame after the scaling, the case for each dimension are mutually exclusive
		if rect.maxX > targetFrame.maxX {
			xDelta = rect.maxX - targetFrame.maxX
		}
		if rect.maxY > targetFrame.maxY {
			yDelta = rect.maxY - targetFrame.maxY
		}
		return AnimatedTransition(scaleDelta: targetScale - scale, xDelta: xDelta, yDelta: yDelta)
	}

	// Performs a simultaneous zoom and pan as part of a programmatic animation
	public func adjust(scaleDelta: Double, xDelta: Double, yDelta: Double) {
		guard let scaleBase = scaleBase, let zoomCentre = zoomCentre else {
			return
		}
		scale = scaleBase + scaleDelta
		origin = zoomCentre - RealPoint(x: realDistance(for: frame.width / 2), y: realDistance(for: frame.height / 2)) + RealPoint(x: xDelta, y: yDelta)
	}

}
