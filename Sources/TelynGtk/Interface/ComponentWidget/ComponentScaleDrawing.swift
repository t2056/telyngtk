import Foundation
import TelynCore
import Cairo
import Gtk
import GtkKit

let scaleDimen: CGFloat = 36;

let scaleMargin: CGFloat = 8;

let majorMarkerLength: CGFloat = 10

let minorMarkerLength: CGFloat = 8

// Handles the drawing of the scale.
internal extension ComponentWidget {
	func drawScale(on context: ContextProtocol) {
		let corner = BezierPath()
		// BezierPath(CGRect(x: 0, y: 0, width: scaleDimen, height: scaleDimen))
		corner.addRect(CGRect(x: 0, y: 0, width: scaleDimen, height: scaleDimen))
		scaleBackgroundColor().set(on: context)
		corner.fill(on: context)
		drawHorizontalScale(on: context)
		drawVerticalScale(on: context)
	}

	func drawHorizontalScale(on context: ContextProtocol) {
		guard let renderContext = renderContext else {
			return;
		}
		// Draw the background
		let background = BezierPath()
		// BezierPath(rect: CGRect(x: scaleDimen, y: 0, width: frame.width - scaleDimen, height: scaleDimen))swiu
		background.addRect(CGRect(x: scaleDimen, y: 0, width: CGFloat(allocatedWidth) - scaleDimen, height: scaleDimen))
		scaleBackgroundColor().set(on: context)
		background.fill(on: context)
		// Draw the frame
		let frameLine = BezierPath()
		frameLine.move(to: CGPoint(x: scaleDimen, y: scaleDimen))
		frameLine.addLine(to: CGPoint(x: CGFloat(allocatedWidth), y: scaleDimen))
		frameLine.lineWidth = 4
		frameLine.lineCapStyle = .round
		scaleBorderColor().set(on: context)
		frameLine.stroke(on: context)
		// Draw the scale labels.
		let interval = renderContext.gridLineInterval();
		for point in stride(from: renderContext.gridLeftBound(for: interval),
							to: renderContext.gridRightBound(for: interval),
							by: interval) {
			let x = renderContext.cgPoint(for: RealPoint(x: point, y: 0)).x
			let bottomY: CGFloat = scaleDimen
			let topY = bottomY - majorMarkerLength
			if x >= scaleDimen {
			let path = BezierPath()
				path.move(to: CGPoint(x: x, y: bottomY))
				path.addLine(to: CGPoint(x: x, y: topY))
				path.lineWidth = 2
				path.lineCapStyle = .round
				majorMarkerColor(for: point).set(on: context)
				path.stroke(on: context)
			}
			// Center the text at the x point and half way between the top of the view and the end of the scsae line
			let centerTextPoint = CGPoint(x: x, y: topY / 2)
			// Draw the string centered at the point and the center line of the scale, with the opacity for that point.
			let string = "\(Int(point))"
			let extents = context.textExtents(string)
			let renderPoint = CGPoint(x: centerTextPoint.x - (CGFloat(extents.width) / 2), y: centerTextPoint.y + (CGFloat(extents.height) / 2))
			context.moveTo(Double(renderPoint.x), Double(renderPoint.y))
			scaleLabelColor(for: point).set(on: context)
			context.showText(string)
			if renderContext.displaysMinorPoints() {
				for minorPoint in stride(from: point + 2, to: point + interval, by: 2) {
					let minorX = renderContext.cgPoint(for: RealPoint(x: minorPoint, y: 0)).x
					let minorTopY = bottomY - minorMarkerLength
					let minorPath = BezierPath()
					if minorX >= scaleDimen {
						minorPath.move(to: CGPoint(x: minorX, y: bottomY))
						minorPath.addLine(to: CGPoint(x: minorX, y: minorTopY))
						minorPath.lineWidth = 2
						minorPath.lineCapStyle = .round
						minorMarkerColor().set(on: context)
						minorPath.stroke(on: context)
					}
				}
			}
		}
	}

	func drawVerticalScale(on context: ContextProtocol) {
		guard let renderContext = renderContext else {
			return;
		}
		// Draw the background
		let background = BezierPath()
		background.addRect(CGRect(x: 0, y: scaleDimen, width: scaleDimen, height: CGFloat(allocatedHeight) + scaleDimen))
		scaleBackgroundColor().set(on: context)
		background.fill(on: context)
		let frameLine = BezierPath()
		frameLine.move(to: CGPoint(x: scaleDimen, y: scaleDimen))
		frameLine.addLine(to: CGPoint(x: scaleDimen, y: CGFloat(allocatedHeight)))
		scaleBorderColor().set(on: context)
		frameLine.lineWidth = 4
		frameLine.lineCapStyle = .round
		frameLine.stroke(on: context)
		let interval = renderContext.gridLineInterval();
		for point in stride(from: renderContext.gridUpperBound(for: interval),
							to: renderContext.gridLowerBound(for: interval),
							by: interval) {
			let y = renderContext.cgPoint(for: RealPoint(x: 0, y: point)).y
			let rightX = scaleDimen
			let leftX = scaleDimen - majorMarkerLength
			if y >= scaleDimen {
			let path = BezierPath()
				path.move(to: CGPoint(x: rightX, y: y))
				path.addLine(to: CGPoint(x: leftX, y: y))
				path.lineWidth = 2
				path.lineCapStyle = .round
				majorMarkerColor(for: point).set(on: context)
				path.stroke(on: context)
			}
			let centerTextPoint = CGPoint(x: leftX / 2, y: y)
			let string = "\(Int(point))"
			let extents = context.textExtents(string)
			let renderPoint = CGPoint(x: centerTextPoint.x - (CGFloat(extents.width) / 2), y: centerTextPoint.y + (CGFloat(extents.height) / 2))
			scaleLabelColor(for: point).set(on: context)
			context.moveTo(Double(renderPoint.x), Double(renderPoint.y))
			context.showText(string)
			if renderContext.displaysMinorPoints() {
				for minorPoint in stride(from: point + 2, to: point + interval, by: 2) {
					let minorY = renderContext.cgPoint(for: RealPoint(x: 0, y: minorPoint)).y
					let minorLeftX = rightX - minorMarkerLength
					if minorY >= scaleDimen {
						let minorPath = BezierPath()
						minorPath.move(to: CGPoint(x: rightX, y: minorY))
						minorPath.addLine(to: CGPoint(x: minorLeftX, y: minorY))
						minorPath.lineWidth =  2
						minorPath.lineCapStyle = .round
						minorMarkerColor().set(on: context)
						minorPath.stroke(on: context)
					}
				}
			}
		}
	}

	func scaleBackgroundColor() -> Color {
		if self.isDarkTheme() {
			return Color.systemNeutral8
		} else {
			return Color.systemNeutral1
		}
	}

	func scaleBorderColor() -> Color {
		if self.isDarkTheme() {
			return Color.systemNeutral4
		} else {
			return Color.systemNeutral5
		}
	}

	func scaleLabelColor(for point: Double) -> Color {
		if self.isDarkTheme() {
			return Color.systemNeutral3.withAlphaComponent(renderContext?.opacity(for: point) ?? 0)
		} else {
			return Color.systemNeutral6.withAlphaComponent(renderContext?.opacity(for: point) ?? 0)
		}
	}

	func majorMarkerColor(for point: Double) -> Color {
		if self.isDarkTheme() {
			return Color.systemNeutral3.withAlphaComponent(renderContext?.opacity(for: point) ?? 0)
		} else {
			return Color.systemNeutral6.withAlphaComponent(renderContext.opacity(for: point) ?? 0)
		}
	}

	func minorMarkerColor() -> Color {
		if isDarkTheme() {
			return Color.systemNeutral4.withAlphaComponent(renderContext?.minorPointOpacity() ?? 0)
		} else {
			return Color.systemNeutral5.withAlphaComponent(renderContext?.minorPointOpacity() ?? 0)
		}
	}

}
