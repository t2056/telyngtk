import Foundation
import TelynCore

let minorPointInterval: Double = 2;

let minimumCgGridLineInterval: CGFloat = 30;

let gridOpacityRange: CGFloat = 50;

let minimumCgMinorGridLineInterval: CGFloat = 20;

let minorGridOpacityRange: CGFloat = 20;

/// This extension contains logic for determining where render lines occur, in real space
public extension RenderContext {

	func gridLeftBound(for interval: Double) -> Double {
		return realPoint(for: CGPoint(x: 0, y: 0)).x.round(downToMultipleOf: interval);
	}

	func gridRightBound(for interval: Double) -> Double {
		return realPoint(for: CGPoint(x: frame.width, y: frame.height)).x.round(upToMultipleOf: interval);
	}

	func gridUpperBound(for interval: Double) -> Double {
		return realPoint(for: CGPoint(x: 0, y: 0)).y.round(downToMultipleOf: interval);
	}

	func gridLowerBound(for interval: Double) -> Double {
		return realPoint(for: CGPoint(x: frame.width, y: frame.height)).y.round(upToMultipleOf: interval);
	}

	// Returns real space distance between major gridlines. Should be a multiple of 10.
	func gridLineInterval() -> Double {
		// Start from and interval of 10.
		var interval: Double = 10;
		// The cg distace between grid line may not be less than cgGridLineInterval, so increment in steps of ten while the calculated cg interval is less than the minimum value.
		while cgDistance(for: interval) < minimumCgGridLineInterval {
			interval = interval * 2;
		}
		return interval;
	}

	// Calculates the maximum member of the squared interval sequence (10, 20, 40, 80) etc, which the point is a multiple of. Used to calculate opacity values for points.
	func maximumInterval(for point: Double) -> Double {
		guard !point.isNaN, !point.isInfinite else {
			return 10;
		}
		let point = Int(point);
		var interval = 10;
		// If point is zero, modulo function has a problem so catch that and return infinity.
		if point == 0 {
			return  Double.infinity;
		}
		while ( point % (interval * 2) ) == 0 {
			interval = interval * 2;
		}
		return Double(interval);
	}


	// Returns the opacity for the grid line at the given point.
	func opacity(for point: Double) -> Double {
		// Calculate the current distance between grid lines at the maximum interval level that this point is displayed at.
		let cgInterval = cgDistance(for: maximumInterval(for: point));
		// Calculate the opacity for this point by calculating the difference from the minimumCgInterval and the calculated cgInterval and dividing that by the opacity range.
		var opacity = (cgInterval - minimumCgGridLineInterval) / gridOpacityRange;
		// Constrain the opacity value to be between 0 and 1.
		if opacity < 0 {
			opacity = 0;
		} else if opacity > 1 {
			opacity = 1;
		}
		return Double(opacity);
	}

	func displaysMinorPoints() -> Bool {
		let cgInterval = cgDistance(for: minorPointInterval)
		return cgInterval >= minimumCgMinorGridLineInterval
	}

	func minorPointOpacity() -> Double {
		let cgInterval = cgDistance(for: minorPointInterval);
		var opacity = (cgInterval - minimumCgMinorGridLineInterval) / minorGridOpacityRange;
		if opacity < 0 {
			opacity = 0;
		} else if opacity > 1 {
			opacity = 1
		}
		return Double(opacity);
	}

}
