import Foundation
import Gdk
import Gtk
import GtkKit
import TelynCore

public let animationInterval = 0.0167

public class AnimatedTransition {

	public var scaleDelta: Double

	public var xDelta: Double

	public var yDelta: Double

	public init(scaleDelta: Double, xDelta: Double, yDelta: Double) {
		self.scaleDelta = scaleDelta
		self.xDelta = xDelta
		self.yDelta = yDelta
	}

}

extension ComponentWidget {

	public func bring(rect: RealRect, into frame: CGRect, animationLength: TimeInterval) {
		animationTransition = renderContext.transitionToBring(rect: rect, into: frame)
		animationStartDate = Date()
		self.animationLength = animationLength
		renderContext.startZoom()
		totalXChange = 0
		totalAnimationLoop = 0
		addTickCallback(animateTransition(_:))
	}

	public func animateTransition(_ widget: WidgetRef) -> Bool {
		guard let animationTransition = animationTransition, let animationStartDate = animationStartDate, let animationLength = animationLength else {
			return false
		}
		let progress = (Date().timeIntervalSince(animationStartDate) / animationLength).ceiling(1)
		let scaleDelta = animationTransition.scaleDelta * progress
		let xDelta = animationTransition.xDelta * progress
		let yDelta = animationTransition.yDelta * progress
		totalXChange += xDelta
		totalAnimationLoop += 1
		renderContext.adjust(scaleDelta: scaleDelta, xDelta: xDelta, yDelta: yDelta)
		// End the animation if we have reached the end point
		if Date().timeIntervalSince(animationStartDate) >= animationLength {
			renderContext.endZoom()
			return false
		}
		queueDraw()
		return true
	}

}
