import Foundation
import Gtk
import GtkKit
import Gdk
import Cairo
import TelynCore
import OpenCombine

let touchZoneStrokeWidth: Double = 48;

public class ComponentWidget: DrawingArea {

	// When the frame is resized, reset the renderContext

	// The upper left and lower right cgPoints.

	private var upperLeft: CGPoint {
		get {
			return CGPoint(x: 0, y: 0);
		}
	}

	private var lowerRight: CGPoint {
		get {
			return CGPoint(x: allocatedWidth, y: allocatedHeight);
		}
	}

	// Rendering variables.

	public var component: Component? = nil {
		didSet {
			if component != oldValue {
				if let renderObserver = renderObserver {
					NotificationCenter.default.removeObserver(renderObserver)
				}
				if let component = component {
					renderContext.adjustToFit(component: component)
					renderObserver = NotificationCenter.default.addObserver(forName: .render, object: nil, queue: nil, using: { [unowned self] (_) in
						queueDraw()
					})
				}
				queueDraw()
			}
		}
	}

	var renderObserver: Any? = nil

	public var renderContext: RenderContext!;

	public var selected: Any? {
		didSet {
			if let selectedNode = selected as? Node {
				previouslySelectedNode = selectedNode;
			}
			queueDraw()
		}
	}

	// The node that was most recently selected. Used to determine which node should be used as nodeA when displaying constraints.
	private var previouslySelectedNode: Node?;

	// Stores the current areas that are tap areas for each element.
	internal var touchLocations: [(path: BezierPath, element: AnyObject)] = []

	// Touch locations for each of the control points.
	internal var lineControlPointTouchLocations: [BezierPath] = [];


	public var delegate: ComponentWidgetDelegate?

	private var panNode: Node? = nil;

	private var movingControlPoint: Int? = nil;

	// Gesture recognisers

	private var tapRecogniser: GestureMultiPress!

	var dragRecogniser: GestureDrag!

	private var zoomRecogniser: GestureZoom!

	private var scrollZoomRecogniser: EventControllerScroll!

	// A frame that represents points within a 10% margin of each edge of the view's frame.
	public var centerFrame: CGRect {
		get {
			let origin = CGPoint(x: Double(allocatedWidth) * 0.1, y: Double(allocatedHeight) * 0.1)
			let size = CGSize(width: Double(allocatedWidth) * 0.8, height: Double(allocatedHeight) * 0.8);
			return CGRect(origin: origin, size: size);
		}
	}

	public var animationTransition: AnimatedTransition?

	public var animationStartDate: Date?

	public var animationLength: TimeInterval?

	public var animationTimer: Timer?

	public var totalXChange: Double = 0

	public var totalAnimationLoop: Int = 0

	public var shouldDrawScale: Bool = true

	public var cancellables = Set<AnyCancellable>()

	public override init() {
		super.init()
		renderContext = RenderContext(in: .zero)
		onDraw(handler: draw(_:_:))
		initialiseGestureRecognisers()
	}

	public required init(raw ptr: UnsafeMutableRawPointer) {
		super.init(raw: ptr)
		renderContext = RenderContext(in: .zero)
		onDraw(handler: draw(_:_:))
		initialiseGestureRecognisers()
	}

	public required init(retainingRaw ptr: UnsafeMutableRawPointer) {
		super.init(retainingRaw: ptr)
		renderContext = RenderContext(in: .zero)
		onDraw(handler: draw(_:_:))
		initialiseGestureRecognisers()
	}

	public func initialiseGestureRecognisers() {
		tapRecogniser = GestureMultiPress(widget: self)
		tapRecogniser.onPressed(handler: onTapPressed(_:presses:x:y:))
		tapRecogniser.onStopped(handler: onTapStopped(_:))
		tapRecogniser.onReleased(handler: onTapReleased(_:presses:x:y:))
		dragRecogniser = GestureDrag(widget: self)
		dragRecogniser.onDragBegin(handler: onDragBegin(_:x:y:))
		dragRecogniser.onDragUpdate(handler: onDragUpdate(_:xOffset:yOffset:))
		dragRecogniser.onDragEnd(handler: onDragEnd(_:xOffset:yOffset:))
		zoomRecogniser = GestureZoom(widget: self)
		zoomRecogniser.onBegin(handler: onZoomBegin)
		zoomRecogniser.onScaleChanged(handler: onZoomUpdated(_:scale:))
		zoomRecogniser.onEnd(handler: onZoomEnd(_:_:))
		scrollZoomRecogniser = EventControllerScroll(widget: self, flags: .bothAxes)
		scrollZoomRecogniser.onScroll(handler: onScrollZoom(_:dx:dy:))
	}

	func bringAnchorIntoFrame(for element: Any?) {
		if let anchor = anchor(for: element), let renderContext = renderContext {
			// Calculate the necessary relative movement of the renderContext's origin necessary to bring the element within the center frame on each axis.
			var xCgDelta: CGFloat = 0;
			if anchor.x < centerFrame.origin.x {
				xCgDelta = centerFrame.origin.x - anchor.x;
			} else if anchor.x > centerFrame.width {
				xCgDelta = centerFrame.width - anchor.x;
			}
			var yCgDelta: CGFloat = 0;
			if anchor.y < centerFrame.origin.y {
				yCgDelta = centerFrame.origin.y - anchor.y;
			} else if anchor.y > centerFrame.height {
				yCgDelta = centerFrame.height - anchor.y;
			}
			renderContext.startPan();
			renderContext.panBy(x: xCgDelta, y: yCgDelta);
		}
	}

	// Gesture recognition functions.

	var tapStopped: Bool = false

	public func onTapPressed(_ recognizer: GestureMultiPressRef, presses: Int, x: Double, y: Double) {
		tapStopped = false
		print("Tap pressed at position: \(x),\(y)")
	}

	public func onTapStopped(_ recongizer: GestureMultiPressRef) {
		print("Tap stopped")
		tapStopped = true
	}

	public func onTapReleased(_ recognizer: GestureMultiPressRef, presses: Int, x: Double, y: Double) {
		print("Tap released at \(x),\(y)")
		guard !tapStopped else {
			return
		}
		// Only recognise a tap if the location is not within the touch zone for a join in the join overlay.
		delegate?.componentWidget(self, didSelectElement: element(at: CGPoint(x: x, y: y)), dueToEventAt: CGPoint(x: x, y: y))
	}

	var dragOrigin: CGPoint?

	public func onDragBegin(_ recogniser: GestureDragRef, x: Double, y: Double) {
		dragOrigin = CGPoint(x: x, y: y)
		if let panNode = element(at: CGPoint(x: x, y: y)) as? Node {
			self.panNode = panNode;
		} else if let touchedControlPointIndex = controlPoint(at: CGPoint(x: x, y: y)) {
			// If the pan began in the touch zone of one of the control points, begin moving that controlPoint.
			self.movingControlPoint = touchedControlPointIndex;
		} else {
			renderContext.startPan();
		}
	}

	public func onDragUpdate(_ recogniser: GestureDragRef, xOffset: Double, yOffset: Double) {
		guard dragOrigin != nil else {
			return
		}
		let position = CGPoint(x: dragOrigin!.x + CGFloat(xOffset), y: dragOrigin!.y + CGFloat(yOffset))
		if let panNode = panNode {
			let realPosition = renderContext.snappedPosition(for: position)
			panNode.move(to: realPosition)
		} else if let movingControlPoint = movingControlPoint {
			if let line = selected as? Line {
				line.setControlPoint(at: movingControlPoint, to: renderContext.snappedPosition(for: position));
			}
		} else {
			renderContext.panBy(x: CGFloat(xOffset), y: CGFloat(yOffset));
		}
		recogniser.set(state: .claimed)
		queueDraw()
	}

	public func onDragEnd(_ recogniser: GestureDragRef, xOffset: Double, yOffset: Double) {
		panNode = nil
		movingControlPoint = nil
		renderContext.endPan()
	}

	public func onZoomBegin(_ recogniser: GestureRef, _ sequence: EventSequenceRef?) {
		renderContext.startZoom()
	}

	public func onZoomUpdated(_ recogniser: GestureZoomRef, scale: Double) {
		renderContext.zoom(by:scale)
		queueDraw()
	}

	public func onZoomEnd(_ recogniser: GestureRef, _ sequence: EventSequenceRef?) {
		renderContext.endZoom()
	}

	public func onScrollZoom(_ controller: EventControllerScrollRef, dx: Double, dy: Double) {
		let scaledelta = 0.05 * -dy
		renderContext.startZoom()
		renderContext.zoom(by: 1 + scaledelta)
		renderContext.endZoom()
		queueDraw()
	}

	// Rendering methods.

	func draw(_ weakSelf: WidgetRef, _ context: ContextRef) -> Bool {
		guard allocatedWidth > 0, allocatedHeight > 0 else {
			return true
		}
		print("draw")
		renderContext.frame = CGRect(origin: .zero, size:  CGSize(width: allocatedWidth, height: allocatedHeight))
		self.touchLocations = [(path: BezierPath, element: AnyObject)]()
		self.lineControlPointTouchLocations = []
		// Draw the background positioning grid.
		drawGrid(on: context)
		drawElements(on: context)
		drawScale(on: context)
		return true
	}

	public func element(at point: CGPoint) -> AnyObject? {
		for (path, element) in self.touchLocations {
			if path.fillContains(point) || path.strokeContains(point) {
				return element;
			}
		}
		return nil;
	}

	// If the point is touching a control point, this functions return the index of that control point. Otherwise, it returns nil.
	public func controlPoint(at point: CGPoint) -> Int? {
		for i in 0..<lineControlPointTouchLocations.count {
			if lineControlPointTouchLocations[i].contains(point) {
				return i;
			}
		}
		return nil;
	}

	public func anchor(for element: Any) -> CGPoint? {
		if let node = element as? Node, node.component == self.component {
			return renderContext.cgPoint(for: node.position);
		} else if let constraint = element as? Constraint, constraint.component == self.component {
			let selectedNode: Node;
			let secondNode: Node;
			if let previouslySelectedNode = previouslySelectedNode, constraint.constrains(node: previouslySelectedNode), let nodeB = otherNode(than: previouslySelectedNode, in: constraint) {
				selectedNode = previouslySelectedNode;
				secondNode = nodeB;
			} else if let nodeA = constraint.nodeA, let nodeB = constraint.nodeB {
				selectedNode = nodeA;
				secondNode = nodeB;
			} else {
				return nil;
			}
			// Obtain the direction for the appropriate node setup.
			let location = constraintLocation(from: selectedNode, to: secondNode, on: constraint.axis);
			switch location {
			case .above, .below:
				// Calculate the x position between the two nodes.
				let xDelta = secondNode.x - selectedNode.x;
				let xPoint = selectedNode.x + (xDelta / 2);
				// Get the cgPoint for that x position and selected node's y position.
				let preDelta = renderContext.cgPoint(for: RealPoint(x: xPoint, y: selectedNode.y));
				// Apply the delta * 3 in the y axis to give the y position of the main line in the constraint display.
				let anchor = CGPoint(x: preDelta.x, y: preDelta.y + (3 * delta(for: location)));
				return anchor;
			case .left, .right:
				// Calculate the y position between the two nodes.
				let yDelta = secondNode.y - selectedNode.y;
				let yPoint = selectedNode.y + (yDelta / 2);
				// Get the cgPoint fo that y position and the selected node's x position.
				let preDelta = renderContext.cgPoint(for: RealPoint(x: selectedNode.x, y: yPoint));
				// Apply the delta * 3 in x axis to give the x position of the main line in the constraint display.
				let anchor = CGPoint(x: preDelta.x + (3 * delta(for: location)), y: preDelta.y);
				return anchor;
			}
		} else if let line = element as? Line, line.component == self.component, let pointA = line.nodeA?.position, let pointB = line.nodeB?.position {
			let delta = (pointB - pointA) / 2;
			let realAnchor = pointA + delta;
			return renderContext.cgPoint(for: realAnchor);
		}
		return nil;
	}

	public func pointInScaleDisplay(_ point: CGPoint) -> Bool {
		return point.x < scaleDimen || point.y < scaleDimen;

	}

}

public extension Widget {
	func isDarkTheme() -> Bool {
		// The compiler won't even launch the app with this.
		// A widget being realized does not mean it is in a valid view hierarchy, but it's a reasonable sanity check
		/**let settings = Settings.getFor(screen: screen)
		let themeValue = settings?.get(property: .gtkThemeName)
		guard let themeValue = themeValue, let themeName = themeValue.getString() else {
			return false
		}
		return themeName.contains("dark")
		*/
		return false
	}

}

public protocol ComponentWidgetDelegate {

	func componentWidget(_ widget: ComponentWidget, didSelectElement element: AnyObject?, dueToEventAt: CGPoint)

}
