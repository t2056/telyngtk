import Foundation
import Gtk
import GtkKit
import Cairo
import TelynCore

extension ComponentWidget {

	func drawGrid(on context: ContextProtocol) {
		drawHorizontalGrid(on: context);
		drawVerticalGrid(on: context);
	}

	func drawHorizontalGrid(on context: ContextProtocol) {
		let interval = renderContext.gridLineInterval();
		// Calculate the left and right bounds of the grid in the real space.
		for majorPoint in stride(from: renderContext.gridLeftBound(for: interval),
								 to: renderContext.gridRightBound(for: interval),
								 by: interval) {
			if interval == 10 {
				for minorPoint in stride(from: majorPoint + minorPointInterval, to: majorPoint + 10, by: minorPointInterval) {
					drawHorizontalGridLine(at: minorPoint, major: false, on: context);
				}
			}
			drawHorizontalGridLine(at: majorPoint, major: true, on: context);
		}
	}

	func drawHorizontalGridLine(at realValueX: Double, major: Bool, on context: ContextProtocol) {
		let path = BezierPath();
		let cgValueX = renderContext.cgPoint(for: RealPoint(x: realValueX, y: 0)).x;
		path.move(to: CGPoint(x: cgValueX, y: scaleDimen));
		path.addLine(to: CGPoint(x: cgValueX, y: CGFloat(allocatedHeight)));
		if major {
			gridLineColor(for: realValueX).set(on: context);
			path.lineWidth = 3;
		} else {
			minorGridLineColor().set(on: context);
			path.lineWidth = 1;
		}
		path.stroke(on: context);
	}

	func drawVerticalGrid(on context: ContextProtocol) {
		let interval = renderContext.gridLineInterval();
		for majorPoint in stride(from: renderContext.gridUpperBound(for: interval),
								 to: renderContext.gridLowerBound(for: interval),
								 by: interval) {
			if interval == 10 {
				for minorPoint in stride(from: majorPoint + 2, to: majorPoint + 10, by: 2) {
					drawVerticalGridLine(at: minorPoint, major: false, on: context)
				}
			}
			drawVerticalGridLine(at: majorPoint, major: true, on: context)
		}
	}

	func drawVerticalGridLine(at realValueY: Double, major: Bool, on context: ContextProtocol) {
		let path = BezierPath();
		let cgValueY = renderContext.cgPoint(for: RealPoint(x: 0, y: realValueY)).y
		path.move(to: CGPoint(x: scaleDimen, y: cgValueY))
		path.addLine(to: CGPoint(x: CGFloat(allocatedWidth), y: cgValueY))
		if major {
			gridLineColor(for: realValueY).set(on: context)
			path.lineWidth = 3
		} else {
			minorGridLineColor().set(on: context)
			path.lineWidth = 1
		}
		path.stroke(on: context)
	}

	func gridLineColor(for point: Double) -> Color {
		if self.isDarkTheme() {
			return Color.systemNeutral3.withAlphaComponent(renderContext.opacity(for: point))
		} else {
			return Color.systemNeutral6.withAlphaComponent(renderContext.opacity(for: point))
		}
	}

	func minorGridLineColor() -> Color {
		if self.isDarkTheme() {
			return Color.systemNeutral4.withAlphaComponent(renderContext.minorPointOpacity())
		} else {
			return Color.systemNeutral5.withAlphaComponent(renderContext.minorPointOpacity())
		}
	}

}
