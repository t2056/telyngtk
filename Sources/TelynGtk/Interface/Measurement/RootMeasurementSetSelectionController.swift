import Foundation
import Gtk
import GtkKit
import TelynCore
import TelynViewModel
import OpenCombine

public class RootMeasurementSetSelectionController<ViewModel: MeasurementSetSelectionViewModel>	: SectionedListController<MeasurementSetSelectionSection, MeasurementSetSelectionItem> {

	public override var bundle: Bundle? {
		return Bundle.module
	}

	public var viewModel = ViewModel()

	var cancellables = Set<AnyCancellable>()

	var sectionCancellables = Set<AnyCancellable>()

	public override func widgetDidLoad() {
		viewModel.sections.onMain() { [unowned self] (sections) in
			setSections(to: sections)
			sectionCancellables = []
			for section in sections {
				viewModel.items(for: section).onMain() { [unowned self] (items) in
					setItems(to: items, in: section)
				}.store(in: &sectionCancellables)
			}
		}.store(in: &cancellables)
		headerbarItem.title = "Select Measurements"
		headerbarItem.startItems = [
			BarButtonItem(title: "Close", onClick: onDismiss(_:))
		]
	}

	public override func generateWidget(for item: MeasurementSetSelectionItem) -> Widget {
		switch item {
			case .available(let measurementSet):
				let row: LinkRow = buildWidget(named: "link_row")
				measurementSet.publisher(for: \.name).bind(to: row.label)
				return row
			case .unavailable(let measurementSet):
				let row: LinkRow = buildWidget(named: "link_row")
				row.image.setFrom(iconName: "emblem-important-symbolic", size: .button)
				measurementSet.publisher(for: \.name).bind(to: row.label)
				return row
			case .placeholder:
				return buildWidget(named: "measurement_placeholder_row")
		}
	}

	public override func title(for section: MeasurementSetSelectionSection) -> String? {
		return viewModel.title(for: section)
	}

	public override func activate(in section: Int, at index: Int) {
		viewModel.selected(in: section, at: index)
	}

	public func onDismiss(_ button: ButtonProtocol) {
		self.dismiss()
	}


}

public class MeasurementSetSelectionController: RootMeasurementSetSelectionController<MeasurementSetSelectionViewModel> {}

public class ExportMeasurementSetSelectionController: RootMeasurementSetSelectionController<ExportMeasurementSetSelectionViewModel> {

	public override func widgetDidLoad() {
		super.widgetDidLoad()
		headerbarItem.title = "Print with..."
	}

}
