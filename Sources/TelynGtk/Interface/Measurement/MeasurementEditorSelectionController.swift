import Foundation
import Gtk
import GtkKit
import OpenCombine
import TelynCore
import TelynViewModel


public class MeasurementEditorSelectionController: SectionedListController<SingleSection, MeasurementEditorSelectionItem> {

	public override var bundle: Bundle? {
		return Bundle.module
	}

	var viewModel = MeasurementEditorSelectionViewModel()

	var cancellables = [AnyCancellable]()

	var sectionCancellables = [AnyCancellable]()

	public override func widgetDidLoad() {
		sectionedList.halign = .fill
		viewModel.sections.onMain() { [unowned self] (sections) in
			setSections(to: sections)
			sectionCancellables = []
			for section in sections {
				viewModel.items(for: section).onMain() { [unowned self] (items) in
					setItems(to: items, in: section)
				}.store(in: &sectionCancellables)
			}
		}.store(in: &cancellables)
		headerbarItem.title = "Measurement Sets"
		headerbarItem.startItems = [
			BarButtonItem(title: "Close", onClick: onClose(_:))
		]
		headerbarItem.endItems = [
			BarButtonItem(iconName: "view-more-symbolic", menu: ActionMenu(children: [
				Action(title: "Import Measurements", handler: onImportMeasurements),
				Action(title: "Export Measurements", handler: onExportMeasurements)
			])),
			BarButtonItem(iconName: "value-increase-symbolic", onClick: onAdd)
		]

		keyActions = [
			KeyAction(input: "n", modifierFlags: .control, action: onAdd),
			KeyAction(input: "i", modifierFlags: .control, action: onImportMeasurements),
			KeyAction(input: "e", modifierFlags: .control, action: onExportMeasurements)
		]
	}

	public override func generateWidget(for item: MeasurementEditorSelectionItem) -> Widget {
		switch item {
			case .measurementSet(let set):
				let row: LinkRow = buildWidget(named: "link_row")
				set.publisher(for: \.name).bind(to: row.label).store(in: &row.cancellables)
				return row
			case .placeholder:
				return buildWidget(named: "measurement_placeholder_row")
		}
	}

	public override func activate(in section: Int, at index: Int) {
		viewModel.select(at: index)
	}

	public func onAdd() {
		viewModel.createMeasurementSet()
	}

	public func onImportMeasurements() {
		viewModel.importMeasurements()
	}

	public func onExportMeasurements() {
		viewModel.exportMeasurements()
	}

	public func onClose(_ button: ButtonProtocol) {
		viewModel.cancel()
	}
}
