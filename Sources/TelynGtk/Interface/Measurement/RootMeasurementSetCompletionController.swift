import Foundation
import Gtk
import GtkKit
import TelynCore
import TelynViewModel
import OpenCombine

public class RootMeasurementSetCompletionController<ViewModel: MeasurementSetCompletionViewModel>: SectionedListController<SingleSection, MeasurementType> {

	public override var bundle: Bundle? {
		return Bundle.module
	}

	public var viewModel = ViewModel()

	var doneButton: GtkKit.BarButtonItem!

	var cancellables = Set<AnyCancellable>()

	public override func widgetDidLoad() {
		model.setSections(to: [.only])
		viewModel.$neededTypes.onMain() { [unowned self] (types) in
			setItems(to: types, in: .only)
		}.store(in: &cancellables)
		doneButton = GtkKit.BarButtonItem(title: "Done", style: .recommended, onClick: onDone(_:))
		headerbarItem.endItems = [doneButton]
		headerbarItem.title = "Complete Measurements"
		viewModel.title.bindAsSubtitle(of: headerbarItem).store(in: &cancellables)
		viewModel.$validated.onMain() { [unowned doneButton] (validated) in
			doneButton!.active = validated
		}.store(in: &cancellables)
	}

	public override func generateWidget(for item: MeasurementType) -> Widget {
		let row: NumberFieldRow = buildWidget(named: "number_field_row")
		row.label.text = item.name
		row.spinbutton.setRange(min: -10_000, max: 10_000)
		row.spinbutton.setIncrements(step: 1, page: 1)
		row.spinbutton.value = 0
		row.onValueChanged() { [weak viewModel] (value) in
			viewModel?.setValue(for: item, to: value)
		}
		return row
	}

	public func onDone(_ button: ButtonProtocol) {
		viewModel.done()
	}

}

public class MeasurementSetCompletionController: RootMeasurementSetCompletionController<MeasurementSetCompletionViewModel> {}

public class ExportMeasurementSetCompletionController: RootMeasurementSetCompletionController<ExportMeasurementSetCompletionViewModel> {

	public override func widgetDidLoad() {
		super.widgetDidLoad()
		doneButton.title = "Print"
	}

}
