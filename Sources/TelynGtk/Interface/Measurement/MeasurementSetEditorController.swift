import Foundation
import Gtk
import GtkKit
import TelynCore
import TelynViewModel
import OpenCombine

public class MeasurementSetEditorController: SectionedListController<SingleSection, MeasurementSetEditorItem> {

	public override var bundle: Bundle? {
		return Bundle.module
	}

	public var viewModel = MeasurementSetEditorViewModel()

	var cancellables = Set<AnyCancellable>()

	var sectionCancellables = Set<AnyCancellable>()

	public override func widgetDidLoad() {
		viewModel.sections.onMain() { [unowned self] (sections) in
			setSections(to: sections)
			sectionCancellables = []
			for section in sections {
				viewModel.items(for: section).onMain() { [unowned self] (items) in
					setItems(to: items, in: section)
				}.store(in: &sectionCancellables)
			}
		}.store(in: &sectionCancellables)
		viewModel.title.bindAsTitle(of: headerbarItem).store(in: &cancellables)
		headerbarItem.endItems = [
			BarButtonItem(iconName: "view-more-symbolic", menu: ActionMenu(children: [
				Action(title: "Delete Measurements", handler: onDeleteMeasurementSet),
				Action(title: "Export Measurements", handler: onExportMeasurements)
			])),
			BarButtonItem(iconName: "value-increase-symbolic", onClick: onAddType)
		]

		keyActions = [
			KeyAction(input: "a", modifierFlags: .control, action: onAddType),
			KeyAction(input: "d", modifierFlags: .control, action: onDeleteMeasurementSet),
			KeyAction(input: "e", modifierFlags: .control, action: onExportMeasurements)
		]
	}

	public override func generateWidget(for item: MeasurementSetEditorItem) -> Widget {
		switch item {
			case .placeholder:
				return buildWidget(named: "measurement_editor_placeholder_row")
			case .measurement(let type):
				let row: NumberFieldRow = buildWidget(named: "number_field_row")
				row.halign = .fill
				row.label.text = type.name
				row.spinbutton.digits = 1
				row.spinbutton.setIncrements(step: 1, page: 1)
				row.spinbutton.setRange(min: -10_000, max: 10_000)
				viewModel.value(for: type).bindToValue(of: row.spinbutton).store(in: &row.cancellables)
				row.onValueChanged() { [unowned viewModel] (value) in
					viewModel.updateValue(for: type, to: value)
				}
				return row
		}
	}

	public func onAddType() {
		viewModel.addType()
	}

	public func onDeleteMeasurementSet() {
		viewModel.deleteMeasurementSet()
	}

	public func onExportMeasurements() {
		viewModel.exportMeasurements()
	}

}
