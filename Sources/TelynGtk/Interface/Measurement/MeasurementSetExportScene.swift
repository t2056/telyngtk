import Foundation
import GtkKit
import TelynViewModel

public class MeasurementSetExportScene: GtkNavigatorCustomScene {

	public var measurementSetExportViewModel = MeasurementSetExportViewModel()

	public init() {
		super.init(viewModel: measurementSetExportViewModel)
	}

	public override func present(from controller: WidgetController) {
		let documentPicker = DocumentPickerController.forSavingFile(ofType: .measurements, title: "Save Measurements...", onFileSelected: onFileSelected(_:))
		documentPicker.confirmButtonTitle = "Export"
		documentPicker.onCancel = { [measurementSetExportViewModel] () in
			measurementSetExportViewModel.didComplete()
		}
		controller.present(documentPicker)
	}

	public func onFileSelected(_ url: URL) {
		guard let data = measurementSetExportViewModel.getData() else {
			return
		}
		do {
			try FileManager.default.createFile(atPath: url.path, contents: nil)
			try data.write(to: url)
			measurementSetExportViewModel.didComplete()
		} catch {
			measurementSetExportViewModel.onError()
		}
	}


}
