import Foundation
import Gtk
import GtkKit
import OpenCombine
import TelynCore
import TelynViewModel

public class AddMeasurementTypeController: SectionedListController<AddMeasurementSection, AddMeasurementItem> {

	public override var bundle: Bundle? {
		return Bundle.module
	}

	var viewModel = AddMeasurementTypeViewModel()

	var confirmItem: GtkKit.BarButtonItem?

	var cancellables = Set<AnyCancellable>()

	var sectionCancellables = Set<AnyCancellable>()

	public override func widgetDidLoad() {
		headerbarItem.title = "Add Measurement"

		viewModel.sections.onMain { [unowned self] (sections) in
			setSections(to: sections)
			for section in sections {
				viewModel.items(in: section).onMain { (items) in
					setItems(to: items, in: section)
				}.store(in: &sectionCancellables)
			}
		}.store(in: &cancellables)

		confirmItem = BarButtonItem(title: "Add", style: .recommended) { [weak viewModel] (_) in
			viewModel?.complete()
		}
		headerbarItem.endItems = [confirmItem!]

		viewModel.$canComplete.onMain() { [confirmItem] (canComplete) in
			confirmItem?.active = canComplete
		}.store(in: &cancellables)
	}

	public override func generateWidget(for item: AddMeasurementItem) -> Widget {
		switch item {
			case .name:
				let row: MeasurementNameRow = buildWidget(named: "measurement_name_row")
				row.entry.placeholderText = "Measurement Name"
				row.entry.onNotifyText { [weak viewModel] (entry, _) in
					viewModel?.updateMeasurementName(to: entry.text)
				}
				row.entry.onActivate() { [weak viewModel] (_) in
					viewModel?.complete()
				}
				return row
			case .suggestion(let measurementType):
				let row: LinkRow = buildWidget(named: "link_row")
				row.label.text = viewModel.title(for: item)
				return row
		}
	}

	public override func title(for section: AddMeasurementSection) -> String? {
		return viewModel.title(for: section)
	}

	public override func activate(in section: AddMeasurementSection, for item: AddMeasurementItem) {
		switch item {
			case .suggestion(let measurementType):
				viewModel.selectSuggestion(type: measurementType)
			default:
				break
		}
	}

}

public class MeasurementNameRow: ListBoxRow {

	public lazy var entry: Entry = child(named: "measurement_name_entry")

}
