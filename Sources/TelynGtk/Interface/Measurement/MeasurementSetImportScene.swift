import Foundation
import GtkKit
import TelynViewModel

public class MeasurementSetImportScene: GtkNavigatorCustomScene {

	let measurementSetImportViewModel = MeasurementSetImportViewModel()

	public init() {
		super.init(viewModel: measurementSetImportViewModel)
	}

	public override func present(from controller: WidgetController) {
		let documentPicker = DocumentPickerController.forOpeningFile(ofType: .measurements, title: "Import Measurements", onFileSelected: onFileSelected(_:))
		documentPicker.confirmButtonTitle = "Import"
		documentPicker.onCancel = { [measurementSetImportViewModel] () in
			measurementSetImportViewModel.didComplete()
		}
		controller.present(documentPicker)
	}

	public func onFileSelected(_ url: URL) {
		let data = try? Data(contentsOf: url)
		measurementSetImportViewModel.importMeasurements(from:data)
	}

}
