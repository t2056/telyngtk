import Foundation
import Gtk
import GtkKit
import OpenCombine
import TelynCore
import TelynViewModel

public class MeasurementSetCreationController: SectionedListController<MeasurementSetCreationSection, MeasurementSetCreationItem> {

	public override var bundle: Bundle? {
		return Bundle.module
	}

	public var viewModel = MeasurementSetCreationViewModel()

	var createBarItem: GtkKit.BarButtonItem?

	var cancellables = Set<AnyCancellable>()

	var sectionCancellables = Set<AnyCancellable>()

	public override func widgetDidLoad() {
		viewModel.sections.onMain() { [unowned self] (sections) in
			setSections(to: sections)
			sectionCancellables = Set<AnyCancellable>()
			for section in sections {
				viewModel.items(for: section).onMain() { [unowned self] (items) in
					setItems(to: items, in: section)
				}.store(in: &sectionCancellables)
			}
		}.store(in: &cancellables)
		createBarItem = GtkKit.BarButtonItem(title: "Create", style: .recommended, onClick: onCreate(_:))
		headerbarItem.title = "New Measurement Set"
		headerbarItem.endItems = [createBarItem!]
		viewModel.canCreate.onMain () { [weak self] (canCreate) in
			self?.createBarItem?.active = canCreate
		}.store(in: &cancellables)
	}

	public override func generateWidget(for item: MeasurementSetCreationItem) -> Widget {
		switch item {
			case .name:
				let row: EntryRow = buildWidget(named: "entry_row")
				row.halign = .fill
				row.label.text = "Name"
				row.entry.onNotifyText() { [weak viewModel] (_, _) in
					viewModel?.name = row.entry.text
				}
				return row
			case .empty:
				let row: SelectableRow = buildWidget(named: "selectable_row")
				row.halign = .fill
				row.label.text = "From Blank"
				viewModel.templateSelectedPublisher(for: item).onMain() { [unowned row](selected) in
					row.selectionIndicator.set(visible: selected)
				}.store(in: &row.cancellables)
				return row
			case .template(let template):
				let row: SelectableRow = buildWidget(named: "selectable_row")
				row.halign = .fill
				row.label.text = template.name
				viewModel.templateSelectedPublisher(for: item).onMain() { [unowned row] (selected) in
					row.selectionIndicator.set(visible: selected)
				}.store(in: &row.cancellables)
				return row
		}
	}

	public override func title(for section: MeasurementSetCreationSection) -> String? {
		return viewModel.title(for: section)
	}
	public override func activate(in section: MeasurementSetCreationSection, for item: MeasurementSetCreationItem) {
		viewModel.selectTemplate(from: item)
	}

	public func onCreate(_ button: ButtonProtocol) {
		viewModel.create()
	}

}
