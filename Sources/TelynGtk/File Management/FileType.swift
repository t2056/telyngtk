import GtkKit

public extension FileType {

	static let pattern = FileType(title: "Pattern", fileExtension: "pattern")

	static let measurements = FileType(title: "Measurement Sets", fileExtension: "measurements")

	static let blocks = FileType(title: "Blocks", fileExtension: "blocks")

	static let pdf = FileType(title: "PDF", fileExtension: "pdf")

}
