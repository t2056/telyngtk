import Foundation
import GLib
import TelynCore
import TelynViewModel
import OpenCombine
import OpenCombineFoundation
import OpenCombineDispatch

public class PatternDocument: PatternProvider {

	public let pattern: Pattern!

	public let url: URL

	public var needsSave: Bool = false

	var cancellables = Set<AnyCancellable>()

	public init(newNamed name: String, at url: URL) {
		pattern = Pattern(withName: name, measurements: MeasurementSet(named: ""))
		self.url = url
		save()
		setup()
	}

	public init?(fromFileAt path: String) {
		guard let data = FileManager.default.contents(atPath: path) else {
			return nil
		}
		guard let pattern = Pattern.decode(data: data) else {
			return nil
		}
		self.pattern = pattern
		url = URL(fileURLWithPath: path)
		setup()
	}

	public func setup() {
		timeout(add: 30000, handler: { [weak self] () in
			guard self != nil else {
				return false
			}
			self!.save()
			return true
		})
		NotificationCenter.default.publisher(for: .patternAnyChange).sink() { [unowned self] (notification) in
			guard let object = notification.object as? Pattern, object === pattern else {
				return
			}
			needsSave = true
		}.store(in: &cancellables)
	}

	public func save() {
		guard needsSave else {
			return
		}
		let data = Pattern.encode(pattern: pattern)
		try? data?.write(to: url)
		needsSave = false
	}



}

