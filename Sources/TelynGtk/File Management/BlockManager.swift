import Foundation
import GLib
import Gtk
import GtkKit
import TelynCore
import TelynViewModel
import OpenCombine

public class BlockManager: BlockProvider {

	static var fileName = "user"

	static var fileExtension = "blocks"

	var dataUrl: URL {
		return URL(fileURLWithPath: getUserDataDir(), isDirectory: true)
	}

	var fileUrl: URL {
		return dataUrl.appendingPathComponent(BlockManager.fileName).appendingPathExtension(BlockManager.fileExtension)
	}

	public var database: BlockDatabase?

	internal var needsSave: Bool = false

	var cancellables = Set<AnyCancellable>()

	public init() {}

	public func initialise() {
		load()
		setupAutosave()
	}

	internal func load() {
		if FileManager.default.fileExists(atPath: fileUrl.path) {
			let data = try! Data(contentsOf: fileUrl)
			let collection = BlockCollection.decode(from: data)
			database = BlockDatabase(from: collection)
		} else {
			database = BlockDatabase()
			try? FileManager.default.createDirectory(at: dataUrl, withIntermediateDirectories: false)
			FileManager.default.createFile(atPath: fileUrl.path, contents: try! database!.userData())
		}
	}

	internal func setupAutosave() {
		database!.publisher(for: \.blocks).onMain { [unowned self] (_) in
			needsSave = true
		}.store(in: &cancellables)
		timeout(add: 30000, handler: { [weak self] () in
			guard self != nil else {
				return false
			}
			self!.save()
			return true
		})
	}

	internal func save() {
		if needsSave, let data = try? database?.userData() {
			try? data.write(to: fileUrl)
			needsSave = false
		}
	}

}
