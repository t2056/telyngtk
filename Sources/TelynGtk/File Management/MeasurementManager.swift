import Foundation
import GLib
import TelynCore
import TelynViewModel
import OpenCombine

public class MeasurementManager: MeasurementProvider {

	static let fileName = "user"

	static let filenameExtension = "measurements"

	var dataUrl: URL {
		return URL(fileURLWithPath: getUserDataDir(), isDirectory: true)
	}

	var fileUrl: URL {
		return dataUrl.appendingPathComponent(MeasurementManager.fileName).appendingPathExtension(MeasurementManager.filenameExtension)
	}

	public var database: MeasurementDatabase?

	public var templates: MeasurementTemplateDatabase? = MeasurementTemplateDatabase(bundleUrl: Bundle.module.url(forResource: "measurementTemplates", withExtension: "json"))

	internal var needSave = false

	var cancellables = Set<AnyCancellable>()

	public init() {}

	public func initialise() {
		load()
		setupAutosave()
	}

	public func load() {
		if FileManager.default.fileExists(atPath: fileUrl.path) {
			let data = try! Data(contentsOf: fileUrl)
			let measurementSets = try! MeasurementCollection.decode(from: data)
			database = MeasurementDatabase(from: measurementSets)
		} else {
			database = MeasurementDatabase()
			try? FileManager.default.createDirectory(at: dataUrl, withIntermediateDirectories: false)
			FileManager.default.createFile(atPath: fileUrl.absoluteString, contents: try! database!.userData())
		}
	}

	public func setupAutosave() {
		database?.publisher(for: \.measurementSets).onMain() { [unowned self] (_) in
			needSave = true
		}.store(in: &cancellables)
		NotificationCenter.default.publisher(for: .measurementSetUpdatedForType).onMain() { [unowned self] (_) in
			needSave = true
		}.store(in: &cancellables)
		timeout(add: 30000, handler: { [weak self] () in
			guard self != nil else {
				return false
			}
			self!.save()
			return true
		})
	}

	public func save() {
		if needSave, let data = try? database?.userData()  {
			do {
				try data.write(to: fileUrl)
			} catch {
			}
			needSave = false
		}
	}

}
