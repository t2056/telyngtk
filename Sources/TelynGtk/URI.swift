import Foundation
import Gdk
import Gtk
import GtkKit

public func openURI(_ uri: String, from controller: WidgetController) {
	var windowRef: Gtk.WindowRef? = nil
	if let window = controller.windowController?.window {
		windowRef = Gtk.WindowRef(window)
	}
	try? showURIOnWindow(parent: windowRef, uri: uri, timestamp: UInt32(CURRENT_TIME))
}

public func openManual(from controller: WidgetController) {
	try? openURI("https://telyn.docs.luoja.co", from: controller)
}
