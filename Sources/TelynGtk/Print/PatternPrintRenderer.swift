import Foundation
import GtkKit
import Cairo
import TelynCore

class PrintDrawer {

	public var layout: PrintLayout

	public init(for layout: PrintLayout) {
		self.layout = layout
	}

	public func draw(at page: Int, in context: ContextProtocol) {
		let pageIndex = layout.pageIndex(forPage: page)// Draw the relevant components.
		let lines = layout.lines(forPage: pageIndex);
		let components = layout.components(forPage: layout.pageIndex(forPage: page))
		for line in lines {
			render(line: line, in: context);
		}
		// Draw the margins
		renderMargins(forPage: pageIndex, in: context);
		// Render the labels.
		let labels = layout.labels(forPage: pageIndex);
		for label in labels {
			render(label: label, in: context);
		}
	}

	public func render(line: PrintLine, in context: ContextProtocol) {
		let path = BezierPath();
		Color.black.set(on: context);
		path.lineWidth = 6;
		path.lineCapStyle = .round;
		path.setLineDash([8.0, 8.0], phase: 0);
		// Move path to the origin.
		path.move(to: line.a.asCG);
		// Add the line.
		if line.controlPoints.count == 0 {
			path.addLine(to: line.b.asCG);
		} else if line.controlPoints.count == 1 {
			path.addQuadCurve(to: line.b.asCG, control: line.controlPoints[0].asCG);
		} else {
			path.addCurve(to: line.b.asCG, controlA: line.controlPoints[0].asCG, controlB: line.controlPoints[1].asCG)
		}
		path.stroke(on: context);
	}

	public func renderMargins(forPage page: PageIndex, in context: ContextProtocol) {
		Color.systemNeutral4.set(on: context)
		let borders = BezierPath()
		borders.addRect(CGRect(origin: CGPoint(x: layout.leftMargin, y: layout.upperMargin), size: CGSize(width: layout.rightMargin - layout.leftMargin, height: layout.lowerMargin - layout.upperMargin)))
		borders.stroke(on: context)
		Color.white.set(on: context)
		let leftMargin = BezierPath()
		leftMargin.addRect(CGRect(origin: CGPoint(x: 0, y: 0), size: CGSize(width: layout.leftMargin, height: layout.pageRect.size.height)))
		leftMargin.fill(on: context)
		let rightMargin = BezierPath()
		rightMargin.addRect(CGRect(x: layout.rightMargin, y: 0, width: layout.pageRect.size.width - layout.rightMargin, height: layout.pageRect.size.height))
		rightMargin.fill(on: context)
		let upperMargin = BezierPath()
		upperMargin.addRect(CGRect(x: 0, y: 0, width: layout.pageRect.size.width, height: layout.upperMargin))
		upperMargin.fill(on: context)
		let lowerMargin = BezierPath()
		lowerMargin.addRect(CGRect(x: 0, y: layout.lowerMargin, width: layout.pageRect.size.width, height: layout.pageRect.size.height - layout.lowerMargin))
		lowerMargin.fill(on: context)
	}

	public func render(label: PrintLabel, in context: ContextProtocol) {
		// Draw a circle to make sure the label stands out.
		let circlePath = BezierPath()
		circlePath.addArc(withCenter: label.location.asCG, radius: 16, startAngle: 0, endAngle: 2 * Double.pi, clockwise: true)
		Color.systemNeutral4.withAlphaComponent(0.8).set(on: context);
		circlePath.fill(on: context);
		// The label position is given as its center point, but UIKit requires the upper left, and the position of that varies based on text size. This code calculates the origin based on the center and size.
		let labelOrigin = CGPoint(x: label.location.asCG.x - (label.text.size.width / 2), y: label.location.asCG.y + (label.text.size.height / 2));
		Color.black.set(on: context)
		label.text.draw(at: labelOrigin, in: context);
	}

}

class PatternPrintRenderer: PrintPageRenderer {

	public let pattern: TelynCore.Pattern

	public let measurementSet: MeasurementSet?

	/// The printable rect that was used to calculate the
	public var currentPrintableRect: CGRect?

	public var layout: PrintLayout?;

	public var drawer: PrintDrawer?;

	public override var numberOfPages: Int {
		return layout?.numberOfPages ?? 1;
	}

	public init(for pattern: TelynCore.Pattern, using measurementSet: MeasurementSet? = nil) {
		self.pattern = pattern
		self.measurementSet = measurementSet
	}

	public override func prepare() {
		layout = PrintLayout(from: pattern, using: measurementSet, pageSize: paperRect.asPrint, printSize: printableRect.asPrint)
		drawer = PrintDrawer(for: layout!)
	}

	public override func drawPage(at pageIndex: Int, in printableRect: CGRect, using context: ContextProtocol) {
		drawer?.draw(at: pageIndex, in: context)
	}

}
