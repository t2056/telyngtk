import Foundation
import TelynCore
public extension CGRect {

	var asPrint: PrintRect {
		get {
			let printOrigin = PrintPoint(x: Double(origin.x), y: Double(origin.y))
			let printSize = PrintSize(width: Double(size.width), height: Double(size.height))
			return PrintRect(origin: printOrigin, size: printSize)
		}
	}

}

// Extension for print points and sizes to be converted to their CG equivalents for the purpose of printing. No conversion is necessary, as print points are already in the desired desktop publishing point units.

public extension PrintPoint {

	var asCG: CGPoint {
		get {
			return CGPoint(x: self.x, y: self.y);
		}
	}

}

public extension PrintSize {

	var asCG: CGSize {
		get {
			return CGSize(width: self.width, height: self.height);
		}
	}

}

public extension PrintRect {

	var asCG: CGRect {
		get {
			return CGRect(origin: origin.asCG, size: size.asCG)
		}
	}

}
