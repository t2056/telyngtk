import Foundation
import Gtk
import OpenCombine

public extension Publisher where Output == Double, Failure == Never {

	func bindToValue(of spinButton: SpinButton) -> AnyCancellable {
		self.onMain { [weak spinButton] (output) in
			spinButton?.value = output
		}
	}

}

public extension Publisher where Output == Double?, Failure == Never {

	func bindToValue(of spinButton: SpinButton) -> AnyCancellable {
		self.onMain { [weak spinButton] (output) in
			guard let output = output else {
				return
			}
			spinButton?.value = output
		}
	}

}
