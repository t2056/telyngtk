import Foundation
import Gtk
import OpenCombine

public extension Publisher where Output == [String], Failure == Never {

	func bindToElements(of comboBox: ComboBoxText) -> AnyCancellable {
		return self.onMain { [weak comboBox] (elements) in
			guard let comboBox = comboBox else {
				return
			}
			comboBox.removeAll()
			for element in elements {
				comboBox.append(text: element)
			}
		}
	}
}

public extension Publisher where Output == Int, Failure == Never {

	func bindToActiveElement(of comboBox: ComboBox) -> AnyCancellable {
		return self.onMain { [weak comboBox] (output) in
			guard let comboBox = comboBox, comboBox.active != output else {
				return
			}
			comboBox.active = output
		}
	}

}

public extension Publisher where Output == ([String], Int), Failure == Never {

	func bindToState(of comboBox: ComboBoxText) -> AnyCancellable {
		return self.onMain { [weak comboBox] (state) in
			guard let comboBox = comboBox else {
				return
			}
			let (elements, active) = state
			comboBox.removeAll()
			for element in elements {
				comboBox.append(text: element)
			}
			comboBox.active = active

		}
	}

}
