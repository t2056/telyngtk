 import Foundation
 import GtkKit
 import OpenCombine
 
 public extension Publisher where Failure == Never {
 
 	func onMain(_ handler: @escaping (Output) -> Void) -> AnyCancellable {
 		return self.sink { (output) in
			main { 
				handler(output)
			}	
 		} 
	}
	
}
