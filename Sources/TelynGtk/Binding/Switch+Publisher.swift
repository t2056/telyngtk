import Foundation
import Gtk
import OpenCombine

public extension Publisher where Output == Bool, Failure == Never {

	func bind(to toggle: Switch) -> AnyCancellable {
		onMain { [weak toggle] (output) in
			toggle?.active = output
		}
	}

}
