import Foundation
import GtkKit
import OpenCombine

public extension Publisher where Output == String?, Failure == Never {

	func bindAsTitle(of headerbarItem: HeaderbarItem) -> AnyCancellable {
		return self.onMain { [weak headerbarItem] (output) in
			headerbarItem?.title = output
		}
	}
	
	func bindAsSubtitle(of headerbarItem: HeaderbarItem) -> AnyCancellable {
		return self.onMain { [weak headerbarItem] (output) in
			headerbarItem?.subtitle = output
		}
	}

}

public extension Publisher where Output == String, Failure == Never {

	func bindAsTitle(of headerbarItem: HeaderbarItem) -> AnyCancellable {
		return self.onMain { [weak headerbarItem] (output) in
			headerbarItem?.title = output
		}
	}

	func bindAsSubtitle(of headerbarItem: HeaderbarItem) -> AnyCancellable {
		return self.onMain { [weak headerbarItem] (output) in
			headerbarItem?.subtitle = output
		}
	}

}
