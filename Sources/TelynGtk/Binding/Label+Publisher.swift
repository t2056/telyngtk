import Foundation
import Gtk
import OpenCombine

public extension Publisher where Output == String, Failure == Never {

	func bind(to label: Label) -> AnyCancellable {
		return self.onMain { [weak label] (output) in
			label?.text = output
		}
	}

}

public extension Publisher where Output == String?, Failure == Never {

	func bind(to label: Label) -> AnyCancellable {
		return self.onMain { [weak label] (output) in
			label?.text = output ?? ""
		}
	}

}
