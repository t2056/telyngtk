import Foundation
import Gtk
import GtkKit
import TelynViewModel

public class TelynNavigator: GtkNavigator {

	public override func scene(for identifier: NavigatorIdentifier) -> GtkNavigatorScene? {
		switch identifier {
			case .componentSelection:
				let controller = ComponentSelectionController()
				return GtkNavigatorWidgetControllerScene(controller: controller, viewModel: controller.viewModel)
			case .componentEditor:
				print("Attempting to create component editor scene")
				let controller = ComponentEditorController()
				let sideDetailController = SideDetailController(primaryChild: controller)
				return GtkNavigatorWidgetControllerScene(controller: sideDetailController, viewModel: controller.viewModel)
			case .nodeDetail:
				let controller = NodeDetailController()
				return GtkNavigatorWidgetControllerScene(controller: controller, viewModel: controller.viewModel)
			case .constraintDetail:
				let controller = ConstraintDetailController()
				return GtkNavigatorWidgetControllerScene(controller: controller, viewModel: controller.viewModel)
			case .lineDetail:
				let controller = LineDetailController()
				return GtkNavigatorWidgetControllerScene(controller: controller, viewModel: controller.viewModel)
			case .measurementSetSelector:
				let controller = MeasurementSetSelectionController()
				return GtkNavigatorWidgetControllerScene(controller: controller, viewModel: controller.viewModel)
			case .measurementSetCompletion:
				let controller = MeasurementSetCompletionController()
				return GtkNavigatorWidgetControllerScene(controller: controller, viewModel: controller.viewModel)
			case .measurementSetEditorSelector:
				let controller = MeasurementEditorSelectionController()
				return GtkNavigatorWidgetControllerScene(controller: controller, viewModel: controller.viewModel)
			case .measurementSetCreation:
				let controller = MeasurementSetCreationController()
				return GtkNavigatorWidgetControllerScene(controller: controller, viewModel: controller.viewModel)
			case .measurementSetEditor:
				let controller = MeasurementSetEditorController()
				return GtkNavigatorWidgetControllerScene(controller: controller, viewModel: controller.viewModel)
			case .addMeasurementTypePage:
				let controller = AddMeasurementTypeController()
				return GtkNavigatorWidgetControllerScene(controller: controller, viewModel: controller.viewModel)
			case .blockLibrary:
				let controller = BlockLibraryController()
				return GtkNavigatorWidgetControllerScene(controller: controller, viewModel: controller.viewModel)
			case .blockExport:
				return BlockExportScene()
			case .blockImport:
				let controller = BlockImportController()
				return GtkNavigatorWidgetControllerScene(controller: controller, viewModel: controller.viewModel)
			case .measurementSetExportSelector:
				let controller = ExportMeasurementSetSelectionController()
				return GtkNavigatorWidgetControllerScene(controller: controller, viewModel: controller.viewModel)
			case .meausurementSetExportCompletion:
				let controller = ExportMeasurementSetCompletionController()
				return GtkNavigatorWidgetControllerScene(controller: controller, viewModel: controller.viewModel)
			case .printConfig:
				return PrintScene()
			case .importInstructions:
				return InstructionsImportScene()
			case .viewInstructions:
				return ViewInstructionsScene()
			case .exportMeasurements:
				return MeasurementSetExportScene()
			case .importMeasurements:
				return MeasurementSetImportScene()
			default:
				return nil
		}
	}

}
