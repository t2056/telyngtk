import Foundation
import Gtk
import GtkKit
import TelynViewModel

public class GtkNavigator: Navigator {

	enum Record {
		case controller(Weak<WidgetController>, viewModel: Weak<ViewModel>, presenter: Presenter)
		case customScene(GtkNavigatorCustomScene, viewModel: Weak<ViewModel>, presenter: Presenter)
	}

	enum Presenter {
		case viewModel(Weak<ViewModel>)
		case root
		case none
	}

	var records: [Record]

	public var rootViewController: WidgetController

	public init(withRoot root: WidgetController) {
		rootViewController = root
		records = []
	}

	open func scene(for identifier: NavigatorIdentifier) -> GtkNavigatorScene? {
		return nil
	}

	private func store(scene: GtkNavigatorScene, presentedFrom presenter: Presenter) {
		let record: Record
		if let scene = scene as? GtkNavigatorWidgetControllerScene {
			record = .controller(Weak(scene.controller), viewModel: Weak(scene.viewModel), presenter: presenter)
		} else if let scene = scene as? GtkNavigatorCustomScene {
			record = .customScene(scene, viewModel: Weak(scene.viewModel), presenter: presenter)
		} else {
			return
		}
		records.append(record)
	}

	public func cleanup() {
		print(records)
		records = records.filter() { (record) in
			switch record {
				case .controller(_, let viewModel, _):
					return viewModel.value != nil
				case .customScene(_, let viewModel, _):
					return viewModel.value != nil
			}
		}
		print(records)
	}

	public func clearRecord(for viewModel: ViewModel) {
		guard let index = records.firstIndex(where: { (record) in
			switch record {
				case .controller(_, let elementViewModel, _):
					return elementViewModel === viewModel
				case .customScene(_, let elementViewModel, _):
					return elementViewModel === viewModel
			}
		}) else {
			return
		}
		records.remove(at: index)
	}

	public func controller(using viewModel: ViewModel) -> WidgetController? {
		// Clear any records whose viewmodels have been garbage collected.
		cleanup()
		let record = records.first { (record) -> Bool in
			switch record {
				case .controller(_, let vm, _):
					return viewModel === vm.value
				case .customScene(_, let vm, _):
					return viewModel === vm.value
			}
		}
		switch record {
			case .controller(let controller, _, _):
				return controller.value
			case .customScene(_, _, let presenter):
				switch presenter {
					case .viewModel(let viewModel):
						if let viewModel = viewModel.value {
							return controller(using: viewModel)
						} else {
							return nil
						}
					case .root:
						return rootViewController
					case .none:
						return nil
				}
			case .none:
				return nil
		}
	}

	public func rootInstall(using identifier: NavigatorIdentifier, style: NavigationStyle) {
		guard let scene = scene(for: identifier) else {
			return
		}
		store(scene: scene, presentedFrom: .root)
		scene.viewModel.navigator = self
		transition(to: scene, from: rootViewController, style: style)
	}

	public func transition(to identifier: NavigatorIdentifier, from viewModel: ViewModel, style: NavigationStyle) {
		guard var nextScene = scene(for: identifier) else {
			return
		}
		guard let controller = controller(using: viewModel) else {
			return
		}
		viewModel.prepare(forTransitionTo: nextScene.viewModel)
		nextScene.viewModel.navigator = self
		store(scene: nextScene, presentedFrom: .viewModel(Weak(viewModel)))
		transition(to: nextScene, from: controller, style: style)
	}

	public func transition(to identifier: NavigatorIdentifier, from controller: WidgetController, style: NavigationStyle, setupHandler: @escaping (ViewModel) -> Void) {
		guard let nextScene = scene(for: identifier) else {
			return
		}
		setupHandler(nextScene.viewModel)
		nextScene.viewModel.navigator = self
		store(scene: nextScene, presentedFrom: .none)
		transition(to: nextScene, from: controller, style: style)
	}

	public func transition(to scene: GtkNavigatorScene, from controller: WidgetController, style: NavigationStyle) {
		if let scene = scene as? GtkNavigatorWidgetControllerScene {
			switch style {
			case .show:
				break
			case .push:
				guard let navController = controller.navigationController else {
					return
				}
				navController.push(scene.controller)
			case .modal:
				let navigationController = NavigationController(withRoot: scene.controller)
				controller.present(navigationController)
			case .popover:
				// TODO: Implement show in a popover.
				break
			case .showDetail:
				let navController = NavigationController(withRoot: scene.controller)
				controller.showSecondaryViewController(navController)
			}
		} else if let scene = scene as? GtkNavigatorCustomScene {
			scene.present(from: controller)
		}
	}

	public func back(from viewModel: ViewModel, whenComplete: (() -> Void)?) {
		guard let controller = controller(using: viewModel), let navController = controller.navigationController else {
			return
		}
		if navController.children.count == 1 {
			navController.dismiss()
			whenComplete?()
		} else {
			navController.pop()
			whenComplete?()
		}
	}

	public func dismiss(from viewModel: ViewModel, whenComplete: (() -> Void)?) {
		guard let controller = controller(using: viewModel) else {
			return
		}
		controller.dismiss()
		whenComplete?()
	}

	public func interactionEnded(for viewModel: ViewModel) {
	            clearRecord(for: viewModel)
	}

	public func informationAlert(from viewModel: ViewModel, title: String, message: String?, buttonText: String, onComplete: (() -> Void)?) {
		guard let window = controller(using: viewModel)?.windowController?.window else {
			return
		}
		let dialog = MessageDialog(parent: WindowRef(raw: window.ptr), type: .other, buttons: .none, text: title, secondaryText: message)
		dialog.addButton(buttonText: buttonText, responseID: Int(ResponseType.ok.rawValue))
		dialog.run()
		dialog.destroy()
		onComplete?()
	}

	public func confirmAlert(from viewModel: ViewModel, title: String, message: String?, cancelText: String, confirmText: String, confirmStyle: ActionStyle, onConfirm: (() -> Void)?) {
		let alert = AlertController(title: title, message: message)
		alert.addAction(AlertAction(title: cancelText, style: .cancel))
		alert.addAction(AlertAction(title: confirmText, style: map(style: confirmStyle)) { (_) in
			onConfirm?()
		})
		controller(using: viewModel)?.present(alert)
	}

	public func textInputAlert(from viewModel: ViewModel, title: String, placeholder: String?, defaultText: String?, cancelText: String, confirmText: String, confirmStyle: ActionStyle, onRename: ((String) -> Void)?) {
		let alert = AlertController(title: title, message: nil)
		alert.addAction(AlertAction(title: cancelText, style: .cancel))
		let confirmAction = AlertAction(title: confirmText, style: map(style: confirmStyle), handler: { [unowned alert] (action) in
			guard let text = alert.textEntries.first?.text, !text.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty else {
				return
			}
			onRename?(text)
		})
		alert.addAction(confirmAction)
		alert.addEntry(configurationHandler: { (entry) in
			entry.placeholderText = placeholder
			entry.text = defaultText
			entry.onNotifyText() { (entry, _) in
				confirmAction.isEnabled = !entry.text.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty
			}
		})
		controller(using: viewModel)?.present(alert)
	}

	public func map(style: ActionStyle) -> AlertAction.Style {
		switch style {
			case .normal:
				return .default
			case .suggested:
				return .suggested
			case .destructive:
				return .destructive
		}
	}

}

public protocol GtkNavigatorScene {

	var viewModel: ViewModel {
		get
		set
	}

}

public class GtkNavigatorWidgetControllerScene: GtkNavigatorScene {

	public var viewModel: ViewModel

	public var controller: WidgetController

	public init(controller: WidgetController, viewModel: ViewModel) {
		self.controller = controller
		self.viewModel = viewModel
	}

}

public class GtkNavigatorCustomScene: GtkNavigatorScene {

	public var viewModel: ViewModel

	public var onComplete: (() -> Void)?

	public init(viewModel: ViewModel) {
		self.viewModel = viewModel
	}

	public func present(from controller: WidgetController) {

	}

}

public protocol NavigatorPopoverDelegate {

	func navigator(willPresentAsPopover controller: WidgetController, identifier: NavigatorIdentifier)

}

public class Weak<T: AnyObject> {

	weak var value: T?

	public init(_ value: T?) {
		self.value = value
	}

}
