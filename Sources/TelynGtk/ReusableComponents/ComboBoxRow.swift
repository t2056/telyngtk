import Foundation
import Gtk
import GtkKit
import OpenCombine

public class ComboBoxRow: ListBoxRow {

	public lazy var label: Label = child(named: "label")

	public lazy var comboBox: ComboBoxText = child(named: "combo_box")

	// Sometimes, both the elements and selection will be updated together. This can lead to bugs; The elements should be updated and then the selection, since the latter depends on the former,
	// but the binding library makes this difficult. To prevent bugs, we flag any necessary updates and apply them on the main thread, which should delay the changes until both values are available.
	// Using a tree model would deal with this issue in a simpler way, so consider migrating to that later.

	public var elements: [String] = [] {
		didSet {
			elementsInvalidated = true
			main { [weak self] in
				self?.apply()
			}
		}
	}

	public var selection: Int = -1 {
		didSet {
			selectionInvalidated = true
			main { [weak self] in
				self?.apply()
			}
		}
	}


	public var elementsInvalidated: Bool = false

	public var selectionInvalidated: Bool = false

	private var changeHandler: ((Int) -> Void)?

	var invalidated: Bool = false

	var cancellables = Set<AnyCancellable>()

	public required init(raw ptr: UnsafeMutableRawPointer) {
		super.init(raw: ptr)
		setup()
	}

	public required init(retainingRaw ptr: UnsafeMutableRawPointer) {
		super.init(retainingRaw: ptr)
		setup()
	}

	public func setup() {
		comboBox.onChanged(handler: changed(_:))
	}

	public func setSegments(to segments: [String]) {
		comboBox.removeAll()
		for segment in segments {
			comboBox.append(text: segment)
		}
	}

	public func onChanged(_ handler: @escaping (Int) -> Void) {
		changeHandler = handler
	}

	func changed(_ combobox: ComboBoxRef) {
		if comboBox.active >= 0 {
			selection = comboBox.active
			changeHandler?(selection)
		}
	}

	func apply() {
		if elementsInvalidated {
			comboBox.removeAll()
			for element in elements {
				comboBox.append(text: element)
			}
			elementsInvalidated = false
		}
		if selectionInvalidated {
			comboBox.active = selection
			selectionInvalidated = false
		}
	}

}
