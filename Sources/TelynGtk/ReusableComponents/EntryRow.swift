import Foundation
import Gtk
import OpenCombine

public class EntryRow: ListBoxRow {

		public lazy var label: Label = child(named: "label")

		public lazy var entry: Entry = child(named: "entry")

		public var cancellables = Set<AnyCancellable>()

		public required init(raw: UnsafeMutableRawPointer) {
			super.init(raw: raw)
			becomeSwiftObj()
		}

		public required init(retainingRaw raw: UnsafeMutableRawPointer) {
			super.init(retainingRaw: raw)
			becomeSwiftObj()
		}
}
