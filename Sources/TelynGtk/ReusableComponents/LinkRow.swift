import Foundation
import Gtk
import GtkKit
import OpenCombine

public class LinkRow: ListBoxRow {

	public lazy var label: Label = child(named: "label")

	public lazy var image: Image = child(named: "image")

	var cancellables = Set<AnyCancellable>()

	public required init(raw ptr: UnsafeMutableRawPointer) {
		super.init(raw: ptr)
		becomeSwiftObj()
	}

	public required init(retainingRaw ptr: UnsafeMutableRawPointer) {
		super.init(retainingRaw: ptr)
		becomeSwiftObj()
	}


}

