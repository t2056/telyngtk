import Foundation
import Gtk

public class SimpleTitleHeader: Box {

	public lazy var titleLabel: Label = child(named: "title_label")

	public required init(raw: UnsafeMutableRawPointer) {
		super.init(raw: raw)
		becomeSwiftObj()
	}

	public required init(retainingRaw raw: UnsafeMutableRawPointer) {
		super.init(retainingRaw: raw)
		becomeSwiftObj()
	}

}
