import Foundation
import Gtk
import GtkKit
import OpenCombine

public class ToggleRow: ListBoxRow {

	public lazy var label: Label = child(named: "label")

	public lazy var toggle: Switch = child(named: "toggle")

	public var cancellables = Set<AnyCancellable>()

	public required init(raw: UnsafeMutableRawPointer) {
		super.init(raw: raw)
		becomeSwiftObj()
	}

	public required init(retainingRaw raw: UnsafeMutableRawPointer) {
		super.init(retainingRaw: raw)
		becomeSwiftObj()
	}

}
