import Foundation
import Gtk
import GtkKit
import OpenCombine

public class NumberFieldRow: ListBoxRow {

	public lazy var label: Label = child(named: "row_label")

	public lazy var spinbutton: SpinButton = child(named: "spinbutton")

	public var cancellables = Set<AnyCancellable>()

	public var changeValueHandler: ((Double) -> Void)?

	public var maxSignificantDigits: Int = 2

	public required init(raw ptr: UnsafeMutableRawPointer) {
		super.init(raw: ptr)
		becomeSwiftObj()
		setup()
	}

	public required init(retainingRaw ptr: UnsafeMutableRawPointer) {
		super.init(retainingRaw: ptr)
		becomeSwiftObj()
		setup()
	}

	public func setup() {
		// Allow the spin button to display arbitrary
		spinbutton.digits = 20
		spinbutton.onValueChanged(handler: valueChanged(_:))
		spinbutton.onOutput(handler: format(_:))
	}

	public func valueChanged(_ spinbutton: SpinButtonRef) {
		changeValueHandler?(spinbutton.value)
	}

	public func onValueChanged(_ handler: @escaping (Double) -> Void) {
		changeValueHandler = handler
	}

	public func format(_ spinbutton: SpinButtonRef) -> Bool {
		let formatter = NumberFormatter()
		formatter.alwaysShowsDecimalSeparator = false
		formatter.minimumSignificantDigits = 0
		// Due to the imprecision of floating point numbers, too many significant digits will cause an excess of digits to be displayed. Consider migrating to Decimals
		formatter.maximumSignificantDigits = 10
		let text = formatter.string(for: spinbutton.value)
		spinbutton.set(text: text)
		return true
	}
	// We use a custom formatter for two reasons:
	// One, it eliminate insignificant digits
	// Two, with the default rounding implementation, if a value more precision than displayed is set on a spinbutton, then that spinbutton will send a signal for the initial rounded value and the updated value when the user alters the value
	// Due to the implementation of bindings, updates from the data layer are not sent until the next main loop. This causes these updates to be repeated, which itself queues a third set of updates, and so on, causing an infinite loop.

}
