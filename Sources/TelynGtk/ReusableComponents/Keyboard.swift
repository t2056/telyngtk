import Foundation
import Gdk
import Gtk

public func isCtrlPressed(using widget: WidgetProtocol) -> Bool {
	guard let display = widget.display, let keymap = Keymap.getFor(display: display) else {
		return false
	}
	// The keymap isn't retained when retrieved, but a release is performed when the wrapper is garbage collected, leading to erroneous deinitialisation
	// We perform a retain manually to correct this
	keymap.ref()
	let modifierState = keymap.modifierState
	// Modifier state is a bitmask and  has a value of 4. We use a bitwise and to check this
	return modifierState & 4 == 4
}
