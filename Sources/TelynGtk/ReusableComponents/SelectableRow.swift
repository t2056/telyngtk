import Foundation
import Gtk
import OpenCombine

public class SelectableRow: ListBoxRow {

	public lazy var label: Label = child(named: "label")

	public lazy var selectionIndicator: Image = child(named: "selection_indicator")

	var cancellables = Set<AnyCancellable>()

	public required init(raw: UnsafeMutableRawPointer) {
		super.init(raw: raw)
		becomeSwiftObj()
	}

	public required init(retainingRaw raw: UnsafeMutableRawPointer) {
		super.init(retainingRaw: raw)
		becomeSwiftObj()
	}

}
