import Foundation
import Gtk
import GtkKit
import TelynViewModel


Application.run(startupHandler: nil) { (app) in
	IconTheme.registerIcons(in: Bundle.module)
	let measurementManager = MeasurementManager()
	measurementManager.initialise()
	measurementProvider = measurementManager
	let blockManager = BlockManager()
	blockManager.initialise()
	blockProvider = blockManager
	let presenter = MainWindowController(application: Application(app))
	presenter.delegate = TelynWindowDelegate()
	presenter.beginPresentation()
}
