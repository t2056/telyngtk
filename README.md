# TelynGtk

This repository contains the code for the Gtk app for Telyn,
a family of software for creating and using parametric sewing patterns. For information on how to install Telyn, 
visit [luoja.co](https://luoja.co)