import XCTest

import TelynGtkTests

var tests = [XCTestCaseEntry]()
tests += TelynGtkTests.allTests()
XCTMain(tests)
