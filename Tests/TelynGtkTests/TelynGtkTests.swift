import XCTest
@testable import TelynGtk

final class TelynGtkTests: XCTestCase {
    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct
        // results.
        XCTAssertEqual(TelynGtk().text, "Hello, World!")
    }

    static var allTests = [
        ("testExample", testExample),
    ]
}
