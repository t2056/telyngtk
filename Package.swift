// swift-tools-version:5.4
// The swift-tools-version declares the minimum version of Swift required to build this package.

import PackageDescription

let package = Package(
    name: "TelynGtk",
    products: [
        // Products define the executables and libraries a package produces, and make them visible to other packages.
        .executable(
            name: "TelynGtk",
            targets: ["TelynGtk"]),
    ],
    dependencies: [
        // Dependencies declare other packages that this package depends on.
        // .package(url: /* package url */, from: "1.0.0"),
        .package(url: "https://github.com/rhx/gir2swift.git", .branch("main")),
        .package(url: "https://github.com/Kiwijane3/GtkKit.git", .branch("master")),
        .package(name: "TelynViewModel", url: "https://gitlab.com/t2056/telynviewmodel.git", .branch("master")),
        .package(url: "https://github.com/OpenCombine/OpenCombine.git", from: "0.12.0")
    ],
    targets: [
        // Targets are the basic building blocks of a package. A target can define a module or a test suite.
        // Targets can depend on other targets in this package, and on products in packages this package depends on.
        .executableTarget(
            name: "TelynGtk",
            dependencies: ["GtkKit", "TelynViewModel", "OpenCombine", .product(name: "OpenCombineDispatch", package: "OpenCombine")],
            resources: [
            	.copy("ui.glade"),
            	.copy("measurementTemplates.json"),
            	.copy("icons")
            ]),
        .testTarget(
            name: "TelynGtkTests",
            dependencies: ["TelynGtk"]),
    ]
)
